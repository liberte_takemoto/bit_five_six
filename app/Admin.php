<?php

	namespace App;

	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Foundation\Auth\User as Authenticatable;
	use Illuminate\Notifications\Notifiable;

	class Admin extends Authenticatable
	{

		use Notifiable;
		use SoftDeletes;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable
			= [
				'name',
				'login_id',
				'password',
				'created_at',
				'updated_at',
				'deleted_at',
			];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden
			= [
				'password',
			];
	}
