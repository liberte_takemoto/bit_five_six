<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Foundation\Auth\User as Authenticatable;
	use Payjp\Subscription;
	use Payjp\Payjp;

	class Customer extends Authenticatable
	{

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable
			= [
				'uid',
				'subscription_id',
				'name1',
				'name2',
				'kana1',
				'kana2',
				'zip1',
				'zip2',
				'addr1',
				'addr2',
				'mail',
				'password',
				'join_date',
				//'first_amount',
				//'regular_amount',
				//'next_order_date',
			    'remember_token',
				'delete_date',
				'created_at',
				'updated_at',
			];
			
			
		/**
		 * @param Request $request
		 *
		 * @return bool
		 */
		static function store(Request $request) {
			DB::beginTransaction();
			try {
				$time = Carbon::now()->timestamp;
				$uid  = str_random(10) . $time;

				Customer::create([ 'uid'             => $uid,
				                   'subscription_id' => '',
				                   'name1'           => $request->input('name1'),
    							   'name2'           => $request->input('name2'),
    							   'kana1'           => $request->input('kana1'),
    							   'kana2'           => $request->input('kana2'),
    							   'zip1'            => $request->input('zip1'),
    							   'zip2'            => $request->input('zip2'),
    							   'addr1'           => $request->input('addr1'),
    							   'addr2'           => $request->input('addr2'),
    							   'mail'            => $request->input('mail'),
    							   'password'        => bcrypt($request->input('password')),
    							   'join_date'       => $request->input('join_date').' 23:59:59',
    							   //'first_amount'    => $request->input('first_amount'),
    							   //'regular_amount'  => $request->input('regular_amount'),
    							   //'next_order_date' => $request->input('next_order_date'),
    							   'remember_token'  => '',
    							   'created_at'      => Carbon::now()]);
				DB::commit();

				return true;
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return bool
		 */
		static function edit(Request $request, $id) {
			DB::beginTransaction();

			try {
    			/*
    			if(!is_null($request->input('delete_date'))){
                    $customer = Customer::find($id);

                    if(!empty($customer->subscription_id)){
        				if (env('APP_ENV') === 'local') {
        					Payjp::setApiKey(config('app.payjp-test-secret-key'));
        				} else {
        					Payjp::setApiKey(config('app.payjp-live-secret-key'));
        				}
                        
                        $sub = Subscription::retrieve($customer->subscription_id);
                        if(!empty($sub)){
                            if($sub->status == "active" || $sub->status == "trial"){
                                $sub->pause();
                            }
                        }
                    }
    			}
    			*/
				Customer::where('id', '=', $id)
					->update([ 'name1'           => $request->input('name1'),
							   'name2'           => $request->input('name2'),
							   'kana1'           => $request->input('kana1'),
							   'kana2'           => $request->input('kana2'),
							   'zip1'            => $request->input('zip1'),
							   'zip2'            => $request->input('zip2'),
							   'addr1'           => $request->input('addr1'),
							   'addr2'           => $request->input('addr2'),
							   'mail'            => $request->input('mail'),
							   'join_date'       => $request->input('join_date').' 23:59:59',
							   //'first_amount'    => $request->input('first_amount'),
							   //'regular_amount'  => $request->input('regular_amount'),
							   //'next_order_date' => $request->input('next_order_date'),
							   'delete_date'     => !is_null($request->input('delete_date')) ? $request->input('delete_date').' 23:59:59' : NULL,
							   'updated_at'      => Carbon::now()]);
							   
                if ($request->input('password') <> '') {
					//パスワードを変更する場合
					Customer::where('id', '=', $id)
							->update([
								        'password' => bcrypt($request->input('password')),
									 ]);
				}
				
				DB::commit();

				return true;
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
		
		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return bool
		 */
		static function editMypage(Request $request, $id) {
			DB::beginTransaction();
            
            if(!is_null($request->input('delete_check'))){
                $delete_date = Carbon::now();
                
                //$customer = Customer::find($id);

                /* 定期購入停止 */
                /*
                if(!empty($customer->subscription_id)){
    				if (env('APP_ENV') === 'local') {
    					Payjp::setApiKey(config('app.payjp-test-secret-key'));
    				} else {
    					Payjp::setApiKey(config('app.payjp-live-secret-key'));
    				}
                    
                    $sub = Subscription::retrieve($customer->subscription_id);
                    $sub->pause();
                }
                */
            }else{
                $delete_date = NULL;
            }

			try {
				Customer::where('id', '=', $id)
					->update([ 'name1'           => $request->input('name1'),
							   'name2'           => $request->input('name2'),
							   'kana1'           => $request->input('kana1'),
							   'kana2'           => $request->input('kana2'),
							   'zip1'            => $request->input('zip1'),
							   'zip2'            => $request->input('zip2'),
							   'addr1'           => $request->input('addr1'),
							   'addr2'           => $request->input('addr2'),
							   'mail'            => $request->input('mail'),
							   'delete_date'     => $delete_date,
							   'updated_at'      => Carbon::now()]);
							   
                if ($request->input('password') <> '') {
					//パスワードを変更する場合
					Customer::where('id', '=', $id)
							->update([
								        'password' => bcrypt($request->input('password')),
									 ]);
				}
				
				DB::commit();

				return true;
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
		
		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return bool
		 */
		static function editMypageEn(Request $request, $id) {
			DB::beginTransaction();
            
            if(!is_null($request->input('delete_check_en'))){
                $delete_date = Carbon::now();
                
                //$customer = Customer::find($id);
                
                /* 定期購入停止 */
                /*
                if(!empty($customer->subscription_id)){
    				if (env('APP_ENV') === 'local') {
    					Payjp::setApiKey(config('app.payjp-test-secret-key'));
    				} else {
    					Payjp::setApiKey(config('app.payjp-live-secret-key'));
    				}
                    
                    $sub = Subscription::retrieve($customer->subscription_id);
                    $sub->pause();
                }
                */
            }else{
                $delete_date = NULL;
            }
            
			try {
				Customer::where('id', '=', $id)
					->update([ 'name1'           => $request->input('name1_en'),
							   'name2'           => $request->input('name2_en'),
							   'kana1'           => $request->input('kana1_en'),
							   'kana2'           => $request->input('kana2_en'),
							   'zip1'            => $request->input('zip1_en'),
							   'zip2'            => $request->input('zip2_en'),
							   'addr1'           => $request->input('addr1_en'),
							   'addr2'           => $request->input('addr2_en'),
							   'mail'            => $request->input('mail_en'),
							   'delete_date'     => $delete_date,
							   'updated_at'      => Carbon::now()]);
							   
                if ($request->input('en_password') <> '') {
					//パスワードを変更する場合
					Customer::where('id', '=', $id)
							->update([
								        'password' => bcrypt($request->input('en_password')),
									 ]);
				}
				
				DB::commit();

				return true;
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
		
		/**
		 * @param Request $request
		 *
		 * @return bool
		 */
		static function EntryCustomer(Request $request) {
			DB::beginTransaction();
			try {
				$time = Carbon::now()->timestamp;
				$uid  = str_random(10) . $time;
    			/*
                $payment = Payment::find(1);
                $dt = Carbon::now();
                */
                
                /* 次回決済日取得 */
                //$next_order_date = $dt->addDay(config('app.free_term'))->toDateString();
                $join_date = Carbon::today()->toDateString();
                
				$customer = Customer::create([ 'uid'             => $uid,
            				                   'subscription_id' => '',
            				                   'name1'           => $request->input('name1'),
                							   'name2'           => $request->input('name2'),
                							   'kana1'           => $request->input('kana1'),
                							   'kana2'           => $request->input('kana2'),
                							   'zip1'            => $request->input('zip1'),
                							   'zip2'            => $request->input('zip2'),
                							   'addr1'           => $request->input('addr1'),
                							   'addr2'           => $request->input('addr2'),
                							   'mail'            => $request->input('mail'),
                							   'password'        => bcrypt($request->input('password')),
                							   'join_date'       => $join_date . ' 23:59:59',
                							   //'first_amount'    => $payment->first_amount,
                							   //'regular_amount'  => $payment->regular_amount,
                							   //'next_order_date' => $next_order_date,
                							   'remember_token'  => '',
                							   'created_at'      => Carbon::now()]);
				DB::commit();

				return [ 'customer_id' => $customer->id ];
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return array();
			}
		}
		
		/**
		 * @param Request $request
		 *
		 * @return bool
		 */
		static function EntryCustomerEn(Request $request) {
			DB::beginTransaction();
			try {
				$time = Carbon::now()->timestamp;
				$uid  = str_random(10) . $time;
    			/*
                $payment = Payment::find(1);

                $dt = Carbon::now();
                */
                /* 次回決済日取得 */
                //$next_order_date = $dt->addDay(config('app.free_term'))->toDateString();
                $join_date = Carbon::today()->toDateString();
 
				$customer = Customer::create([ 'uid'             => $uid,
            				                   'subscription_id' => '',
            				                   'name1'           => $request->input('name1_en'),
                							   'name2'           => $request->input('name2_en'),
                							   'kana1'           => $request->input('kana1_en'),
                							   'kana2'           => $request->input('kana2_en'),
                							   'zip1'            => $request->input('zip1_en'),
                							   'zip2'            => $request->input('zip2_en'),
                							   'addr1'           => $request->input('addr1_en'),
                							   'addr2'           => $request->input('addr2_en'),
                							   'mail'            => $request->input('mail_en'),
                							   'password'        => bcrypt($request->input('en_password')),
                							   'join_date'       => $join_date . ' 23:59:59',
                							   //'first_amount'    => 0,
                							   //'regular_amount'  => $payment->regular_amount,
                							   //'next_order_date' => $next_order_date,
                							   'remember_token'  => '',
                							   'created_at'      => Carbon::now()]);
				DB::commit();

				return [ 'customer_id' => $customer->id ];
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return array();
			}
		}
	}
