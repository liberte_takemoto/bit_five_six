<?php
	/**
	 * Created by IntelliJ IDEA.
	 * User: Kazuki Takemoto
	 * Date: 2018/11/08
	 * Time: 16:00
	 */

	namespace app\Http\Composers;


	use Illuminate\Contracts\View\View;
	use Illuminate\Support\Facades\Auth;

	class HeaderComposer
	{


		/**
		 * MenuComposer constructor.
		 */
		public function __construct() {
			$this->auth = Auth::guard('mypage');
		}

		/**
		 * @param View $view
		 */
		public function compose(View $view) {
			$view->with([ 'auth' => $this->auth ]);
		}
	}