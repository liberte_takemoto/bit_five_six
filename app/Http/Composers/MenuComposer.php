<?php
	/**
	 * Created by IntelliJ IDEA.
	 * User: Kazuki Takemoto
	 * Date: 2018/11/08
	 * Time: 16:00
	 */

	namespace app\Http\Composers;


	use Illuminate\Contracts\View\View;
	use Illuminate\Support\Facades\Auth;

	class MenuComposer
	{

		private $menus;

		/**
		 * MenuComposer constructor.
		 */
		public function __construct() {
			$auth = Auth::guard('admin');
			if ($auth->check()) {
				$this->menus = [ [
									 'name'   => "顧客管理",
									 'router' => 'admin.customer.index',
									 'icon'   => '',
									 'rank'   => 1,
									 'sub'    => null,
								 ],
								 [
									 'name'   => "レポート管理",
									 'router' => 'admin.report.index',
									 'icon'   => "",
									 'rank'   => 2,
									 'sub'    => null,
								 ] ];

			} else {
				$this->menus = null;
			}
		}

		/**
		 * @param View $view
		 */
		public function compose(View $view) {
			$view->with([ 'menus' => $this->menus ]);
		}
	}