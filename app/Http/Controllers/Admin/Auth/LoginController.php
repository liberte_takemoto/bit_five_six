<?php

	namespace App\Http\Controllers\Admin\Auth;

	use App\Http\Controllers\Controller;
	use Illuminate\Foundation\Auth\AuthenticatesUsers;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Http\Request;

	class LoginController extends Controller
	{
		/*
		|--------------------------------------------------------------------------
		| Login Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles authenticating users for the application and
		| redirecting them to your home screen. The controller uses a trait
		| to conveniently provide its functionality to your applications.
		|
		*/

		use AuthenticatesUsers;

		/**
		 * Where to redirect users after login.
		 *
		 * @var string
		 */
		protected $redirectTo = '/admin/customer';

		/**
		 * Create a new controller instance.
		 *
		 */
		public function __construct() {
			$this->middleware('admin.guest')->except('logout');
		}

		public function showLoginForm() {
			return view('admin.auth.login'); //管理者ログインページのテンプレート
		}

		/**
		 *
		 * emailからlogin_idに変更
		 * Get the login username to be used by the controller.
		 *
		 * @return string
		 */
		public function username() {
			return 'login_id';
		}

		public function logout(Request $request) {
			$this->guard()->logout();
			$request->session()->flush();
			$request->session()->regenerate();

			return redirect('/admin');
		}

		protected function guard() {
			return Auth::guard('admin'); //管理者認証のguardを指定
		}

		/**
		 * @param Request $request
		 * @param         $user
		 *
		 * @return $this|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
		 */
		 /*
		public function authenticated(Request $request, $user) {
			$this->incrementLoginAttempts($request);

			$errors = [ $this->username() => trans('auth.failed') ];

			if ($request->expectsJson()) {
				return response()->json($errors, 422);
			}

			$request->session()->flush();

			$request->session()->regenerate();

			return redirect('/login')->withInput($request->only($this->username(), 'remember'))
									 ->withErrors($errors);

			return redirect()->intended($this->redirectPath());
		}
		*/

	}
