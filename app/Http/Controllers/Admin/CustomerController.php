<?php

	namespace App\Http\Controllers\Admin;


	use App\Customer;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Crypt;
	use Illuminate\Support\Facades\DB;


	class CustomerController extends Controller
	{
    	
		/**
		 * CustomerController constructor.
		 */
		public function __construct() {
			$this->middleware('admin.auth');
		}


		/**
		 * 顧客一覧
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index(Request $request) {
			$customers = Customer::orderBy('created_at', 'desc');

            $search = $request->input('search');
            
            if (!empty($search)) {
                $search_name = $request->input('search_name');
                $search_create_start = $request->input('search_create_start');
                $search_create_end   = $request->input('search_create_end');
            
    			if ($search_name <> '') {
    				$customers = $customers->where(DB::raw('CONCAT(`name1`,`name2`,`kana1`,`kana2`,`mail`)'),
    											   'LIKE',
    											   "%{$search_name}%");
    			}
    			
    			if($search_create_start <> ''){
    				$customers = $customers->where('created_at','>=',$search_create_start . '00:00:00');
    			}
    
    			if($search_create_end <> ''){
    				$customers = $customers->where('created_at','<=',$search_create_end . '23:59:59');
    			}
            }
            
            $customers = $customers->paginate(config('app.pager_item'));


			return view('admin.customer.list', [ 'customer_list' => $customers ]);
		}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    		$this->validate($request, [ 'name1'           => 'required|max:50',
    		                            'name2'           => 'required|max:50',
    		                            'kana1'           => 'required|katakana|max:50',
    		                            'kana2'           => 'required|katakana|max:50',
    		                            'zip1'            => 'required|numeric|digits:3',
    		                            'zip2'            => 'required|numeric|digits:4',
    		                            'addr1'           => 'required|max:100',
    		                            'addr2'           => 'required|max:100',
    		                            'mail'            => 'required|email|unique:customers,mail',
    		                            'password'        => 'required|confirmed|min:6',
    		                            'join_date'       => 'required|date']);
    		                            //'first_amount'    => 'required|numeric',
    		                            //'regular_amount'  => 'required|numeric',
    									//'next_order_date' => 'required|date']);

			$insert = Customer::store($request);

			if ($insert) {
				alert()->success('成功!', '顧客の追加に成功しました');
			} else {
				alert()->error('失敗!', '顧客の追加に失敗しました');
			}

			return redirect(url(route('admin.customer.index')));
    }

		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function edit(Request $request, $id) {
			$customer = Customer::find($id);

			return view('admin.customer.edit', [ 'customer' => $customer ]);
		}
		
		
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
    		$this->validate($request, [ 'name1'           => 'required|max:50',
    		                            'name2'           => 'required|max:50',
    		                            'kana1'           => 'required|katakana|max:50',
    		                            'kana2'           => 'required|katakana|max:50',
    		                            'zip1'            => 'required|numeric|digits:3',
    		                            'zip2'            => 'required|numeric|digits:4',
    		                            'addr1'           => 'required|max:100',
    		                            'addr2'           => 'required|max:100',
    		                            'mail'            => 'required|email|unique:customers,mail,'.$id.',id',
    		                            'password'        => 'nullable|confirmed|min:6',
    		                            'join_date'       => 'required|date',
    		                            //'first_amount'    => 'required|numeric',
    		                            //'regular_amount'  => 'required|numeric',
    									//'next_order_date' => 'required|date',
    									'delete_date'     => 'nullable|date']);
    
            $update = Customer::edit($request,$id);
            
    		if($update){
    			alert()->success('成功!','顧客の更新に成功しました');
    		}else{
    			alert()->error('失敗!','顧客の更新に失敗しました');
    		}
    
    		return redirect(url(route('admin.customer.index')));
        }
	}
