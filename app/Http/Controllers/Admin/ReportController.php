<?php

	namespace App\Http\Controllers\Admin;

	use App\Report;
	use App\Customer;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use App\Mail\RegisterReport;
	use Illuminate\Support\Facades\Mail;

	class ReportController extends Controller
	{
    	
		/**
		 * CustomerController constructor.
		 */
		public function __construct() {
			$this->middleware('admin.auth');
		}


		/**
		 * レポート一覧
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index(Request $request) {
			$reports = Report::orderBy('created_at', 'desc');

            $search = $request->input('search');
            
            if (!empty($search)) {
                $search_name = $request->input('search_name');
                $search_create_start = $request->input('search_create_start');
                $search_create_end   = $request->input('search_create_end');
            
    			if ($search_name <> '') {
    				$reports = $reports->where(DB::raw('CONCAT(`title`,`title_en`)'),
    											   'LIKE',
    											   "%{$search_name}%");
    			}
    			
    			if($search_create_start <> ''){
    				$reports = $reports->where('created_at','>=',$search_create_start . '00:00:00');
    			}
    
    			if($search_create_end <> ''){
    				$reports = $reports->where('created_at','<=',$search_create_end . '23:59:59');
    			}
            }
            
            $reports = $reports->paginate(config('app.pager_item'));


			return view('admin.report.list', [ 'report_list' => $reports ]);
		}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('admin.report.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    		$this->validate($request, [ 'title'           => 'required|max:100',
    		                            'title_en'        => 'required|max:100',
    		                            'content'         => 'required|max:5000',
    		                            'content_en'      => 'required|max:5000',
    									'keyword'         => 'required|max:1000']);

			$insert = Report::store($request);

			if (empty($insert) === false) {
    			/* レポート新規登録お知らせメール */
    			$customers = Customer::whereNull('delete_date')
    			                     ->get();

    			foreach($customers AS $customer){
    				$report = Report::find($insert['report_id']);
        			Mail::to($customer->mail)
    				->queue(new RegisterReport($customer,$report));
    			}
				alert()->success('成功!', 'レポートの追加に成功しました');
			} else {
				alert()->error('失敗!', 'レポートの追加に失敗しました');
			}

			return redirect(url(route('admin.report.index')));
    }

		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function edit(Request $request, $id) {
			$report = Report::find($id);
			
			$report_keyword = DB::table('report_keywords')->where('report_id', '=', $id)->get();
			
			foreach($report_keyword AS $key => $var){
    			$keyword[$key] = $var->keyword;
			}
			$report->keyword = implode(",",$keyword);

			return view('admin.report.edit', [ 'report' => $report]);
		}
		
		
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
    		$this->validate($request, [ 'title'           => 'required|max:100',
    		                            'title_en'        => 'required|max:100',
    		                            'content'         => 'required|max:5000',
    		                            'content_en'      => 'required|max:5000',
    									'keyword'         => 'required|max:1000']);
    
            $update = Report::edit($request,$id);
            
    		if($update){
    			alert()->success('成功!','レポートの更新に成功しました');
    		}else{
    			alert()->error('失敗!','レポートの更新に失敗しました');
    		}
    
    		return redirect(url(route('admin.report.index')));
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function uploadImage(Request $request)
        {
			$file_name = uniqid(rand()) . '.jpg';

			$report_image = $request->file('upload')->storeAs('/public/images/report/upload', $file_name);

            if(!is_null($report_image)){
                
    			$image_data = array( 'url' => asset('storage/images/report/upload/'.$file_name),
    								 'uploaded'  => true
                                    );
            }else{
    			$image_data = array( 'error' => array( 'message' => "画像のアップロードに失敗しました"),
    								 'uploaded'  => false,
                                    );
            }
            
			$json = (array)$image_data;
            
            return json_encode($json);
        }
        
    	/**
    	 * Remove the specified resource from storage.
    	 *
    	 * @param  int $id
    	 *
    	 * @return \Illuminate\Http\Response
    	 */
    	public function destroy($id) {
    		$destroy = Report::remove($id);
    
    		if($destroy){
    			alert()->success('成功!','レポートの削除に成功しました');
    		}else{
    			alert()->error('失敗!','レポートの削除に失敗しました');
    		}
    
    		return redirect(url(route('admin.report.index')));
    	}
	}
