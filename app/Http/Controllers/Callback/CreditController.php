<?php

	namespace App\Http\Controllers\Callback;

	use App\Order;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Blade;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Log;
	use Illuminate\Support\Facades\Mail;
	use App\Mail\Credit;
	use App\Mail\RegularCredit;
	use App\Mail\RegularCreditFailed;
	use Payjp\Customer;
	use Payjp\Payjp;

	class CreditController extends Controller
	{
		public function index(Request $request) {
			$content = json_decode($request->getContent(), true);
			if ($request->header('X-Payjp-Webhook-Token') != config('app.webhock_token')) {
				//					abort(500, '認証失敗');
				Log::error('認証失敗');
				Log::error($request->header('X-Payjp-Webhook-Token'));
			}

            /*
			if (empty($content['data']['metadata'])) {
				//テスト送信
				Log::info('テスト送信');
				Log::info($content);
				abort(200, 'テスト送信成功');
			}
			*/

			Log::info('Webhock開始');
			switch ($content['type']) {
				case 'charge.succeeded':
					//支払い成功
					
					if(empty($content['data']['metadata']['order_id']) === false){
    					/* 初回決済 */
        				Log::info('初回決済：'.$content['data']['metadata']['order_id']);
            			/* 決済完了メール送信処理 */
    					$order_id  = $content['data']['metadata']['order_id'];
    					$charge_id = $content['data']['id'];
    					Order::where('id', '=', $order_id)
    						 ->update(
    							 [ 'uid' => $charge_id ]
    						 );
    					$order = Order::find($order_id);
    					
						$customer = DB::table('customers')
        							 ->where('id', '=', $order->customer_id)
        							 ->first();
    					
                        $next_order_date = date('Y-m-d',mktime(0,0,0,date('m'),date('d')+365,date('Y')));
                        
                        /* 次回決済日更新 */
                        DB::table('customers')->where('id', '=', $order->customer_id)
        					->update([ 'next_order_date' => $next_order_date,
        							   'updated_at'      => Carbon::now()]);
    					
        				
            			Mail::to($customer->mail)
        				->queue(new Credit($customer));

					}else if(empty($content['data']['subscription']) === false){
    					/* 継続決済 */
        				Log::info('継続決済：'.$content['data']['subscription']);
    					$subscription_id  = $content['data']['subscription'];
    					$charge_id    = $content['data']['id'];
    					
    					$customer = DB::table('customers')->where('subscription_id', '=', $subscription_id)->first();

                        $next_order_date = date('Y-m-d',mktime(0,0,0,date('m'),date('d')+365,date('Y')));
    					
    					/* ordersテーブルデータ作成 */
        				Order::create([ 'customer_id'          => $customer->id,
            						    'uid'                  => $charge_id,
            						    'order_date'           => Carbon::now(),
            						    'order_price'          => $customer->regular_amount,
            						    'created_at'           => Carbon::now()]);
    					
    					/* customersテーブル次回決済日更新 */
                        DB::table('customers')->where('id', '=', $customer->id)
        					->update([ 'next_order_date' => $next_order_date,
        							   'updated_at'      => Carbon::now()]);
    					
    					/* 決済完了メール送信 */
            			Mail::to($customer->mail)
        				->queue(new RegularCredit($customer));
					}
    				
					break;
					
				case 'charge.failed':
					//支払い失敗
					if(empty($content['data']['metadata']['order_id']) === false){
    					/* 初回決済 */
        				Log::info('初回決済失敗：'.$content['data']['metadata']['order_id']);
    					$order_id  = $content['data']['metadata']['order_id'];
    					/* 支払いデータ取得 */
    					$order = Order::find($order_id);
    					/* 顧客データ取得 */
    					$customer = DB::table('customers')->where('id', '=', $order->customer_id)->first();
    					/* PayJp顧客データ削除 */
        				if (env('APP_ENV') === 'local') {
        					Payjp::setApiKey(config('app.payjp-test-secret-key'));
        				} else {
        					Payjp::setApiKey(config('app.payjp-live-secret-key'));
        				}
                        $payjp_customer = Customer::retrieve($customer->uid);
                        
                        $payjp_customer->delete();
    					
    					Order::where('id', '=', $order_id)->delete();
    					
					}else if(empty($content['data']['subscription']) === false){
    					/* 継続決済 */
        				Log::info('継続決済失敗：'.$content['data']['subscription']);
    					$subscription_id  = $content['data']['subscription'];
    					
    					$customer = DB::table('customers')->where('subscription_id', '=', $subscription_id)->first();
    					
    					/* 決済失敗メール送信 */
            			Mail::to(config('app.administrator_mail'))
        				->queue(new RegularCreditFailed($customer));
					}
					
					break;
					
				case 'subscription.created':
					//定期購入成功
					$customer_id     = $content['data']['metadata']['customer_id'];
					$subscription_id = $content['data']['id'];
					
					/* customersテーブル次回決済日更新 */
                    DB::table('customers')->where('id', '=', $customer_id)
    					->update([ 'subscription_id' => $subscription_id,
    							   'updated_at'      => Carbon::now()]);
					
				    break;
					
			}

			Log::info('Webhock終了');
			echo '1';
			exit;
		}
	}
