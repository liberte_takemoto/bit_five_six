<?php

	namespace App\Http\Controllers;


	use App\Customer;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Crypt;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Http\Request;
	use App\Mail\EntryCustomer;
	use Illuminate\Support\Facades\Mail;
	


	class EntryController extends Controller
	{
    	
		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index($lang) {
            if($lang == "en"){
    			return view('entry.index_en');
            }else{
    			return view('entry.index');
            }
		}
		
		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function thankyou($lang) {
            if($lang == "en"){
    			return view('entry.thankyou_en');
            }else{
    			return view('entry.thankyou');
            }
		}
		
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request,$lang)
        {
            
            if($lang == "en"){
        		$this->validate($request, [ 'name1_en'           => 'required|max:50',
        		                            'name2_en'           => 'required|max:50',
        		                            'kana1_en'           => 'required|katakana|max:50',
        		                            'kana2_en'           => 'required|katakana|max:50',
        		                            'zip1_en'            => 'required|numeric|digits:3',
        		                            'zip2_en'            => 'required|numeric|digits:4',
        		                            'addr1_en'           => 'required|max:100',
        		                            'addr2_en'           => 'required|max:100',
        		                            'mail_en'            => 'required|email|unique:customers,mail',
        		                            'en_password'        => 'nullable|confirmed|min:6'], 
                                          ['name1_en.required'       => 'Please enter',
                                           'name1_en.max'            => 'Please enter with 50 characters',
                                           'name2_en.required'       => 'Please enter',
                                           'name2_en.max'            => 'Please enter with 50 characters',
                                           'kana1_en.required'       => 'Please enter',
                                           'kana1_en.katakana'       => 'Please input in Katakana',
                                           'kana2_en.required'       => 'Please enter',
                                           'kana2_en.katakana'       => 'Please input in Katakana',
                                           'zip1_en.required'        => 'Please enter',
                                           'zip1_en.numeric'         => 'Please enter with numbers',
                                           'zip1_en.digits'          => 'Please enter with 3 numeric characters',
                                           'zip2_en.required'        => 'Please enter',
                                           'zip2_en.numeric'         => 'Please enter with numbers',
                                           'zip2_en.digits'          => 'Please enter with 4 numeric characters',
                                           'addr1_en.required'       => 'Please enter',
                                           'addr1_en.max'            => 'Please enter with 100 characters',
                                           'addr2_en.required'       => 'Please enter',
                                           'addr2_en.max'            => 'Please enter with 100 characters',
                                           'mail_en.required'        => 'Please enter',
                                           'mail_en.email'           => 'Please enter in the form of e-mail address',
                                           'mail_en.unique'          => 'It is already registered email address',
                                           'en_password.required'    => 'Please enter',
                                           'en_password.confirmed'   => 'Passwords do not match',
                                           'en_password.min'         => 'Please enter at least 6 letters']);
        									
                $update = Customer::EntryCustomerEn($request);
                
            }else{
        		$this->validate($request, [ 'name1'           => 'required|max:50',
        		                            'name2'           => 'required|max:50',
        		                            'kana1'           => 'required|katakana|max:50',
        		                            'kana2'           => 'required|katakana|max:50',
        		                            'zip1'            => 'required|numeric|digits:3',
        		                            'zip2'            => 'required|numeric|digits:4',
        		                            'addr1'           => 'required|max:100',
        		                            'addr2'           => 'required|max:100',
        		                            'mail'            => 'required|email|unique:customers,mail',
        		                            'password'        => 'nullable|confirmed|min:6']);
                
                $update = Customer::EntryCustomer($request);
            }
            
            if($lang == "en"){
                /* 英語 */
        		if(empty($update) === false){
                    /* 会員登録完了メール */
    				$customer = Customer::find($update['customer_id']);
        			Mail::to($customer->mail)
    				->queue(new EntryCustomer($customer));
            		
        			return view('entry.thankyou_en', [ 'error' => 0 ]);
        		}else{
        			return view('entry.thankyou_en', [ 'error' => 1 ]);
        		}
            }else{
                /* 日本語 */
        		if(empty($update) === false){
                    /* 会員登録完了メール */
    				$customer = Customer::find($update['customer_id']);
        			Mail::to($customer->mail)
    				->queue(new EntryCustomer($customer));
    				
        			return view('entry.thankyou', [ 'error' => 0 ]);
        		}else{
        			return view('entry.thankyou', [ 'error' => 1 ]);
        		}
            }
        }
	}
