<?php

	namespace App\Http\Controllers;

	use App\Type;
	use Illuminate\Http\Request;

	class HomeController extends Controller
	{
		public function top() {
			return view('index.top');
		}
    	
		public function law($lang) {
    		if($lang == "en"){
    			return view('info.law_en');
    		}else{
    			return view('info.law');
    		}
		}

		public function policy($lang) {
    		if($lang == "en"){
    			return view('info.policy_en');
    		}else{
    			return view('info.policy');
    		}
		}

		public function rule($lang) {
    		if($lang == "en"){
    			return view('info.rule_en');
    		}else{
    			return view('info.rule');
    		}
		}
	}
