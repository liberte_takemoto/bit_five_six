<?php

	namespace App\Http\Controllers\Mypage\Auth;

	use App\Http\Controllers\Controller;
	use Illuminate\Foundation\Auth\AuthenticatesUsers;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Http\Request;
	use Carbon\Carbon;
	use App\Customer;
	use App\Order;

	class LoginController extends Controller
	{
		/*
		|--------------------------------------------------------------------------
		| Login Controller
		|--------------------------------------------------------------------------
		|
		| This controller handles authenticating users for the application and
		| redirecting them to your home screen. The controller uses a trait
		| to conveniently provide its functionality to your applications.
		|
		*/

		use AuthenticatesUsers;

		/**
		 * Where to redirect users after login.
		 *
		 * @var string
		 */
		protected $redirectTo = '/mypage/report/jp';

		/**
		 * Create a new controller instance.
		 *
		 */
		public function __construct() {
			$this->middleware('mypage.guest')->except('logout');
		}
        
		public function login(Request $request,$lang) {
    		
			//バリデーション
            
            if($lang == "en"){
    			$this->validate($request, [ 'mail_en'     => 'required|exists:customers,mail',
    										'en_password' => 'required' ], 
                                          ['mail_en.required'     => 'Please enter',
                                            'mail_en.exists'      => 'The email address you entered is incorrect.',
                                           'en_password.required' => 'Please enter']);
                
    			$mail     = $request->input('mail_en');
    			$password = $request->input('en_password');
            }else{
    			$this->validate($request, [ 'mail'     => 'required|exists:customers,mail',
    										'password' => 'required' ]);
    										
    			$mail     = $request->input('mail');
    			$password = $request->input('password');
            }
            
			if (Auth::guard('mypage')->validate([ 'mail'          => $mail,
												  'password'      => $password])) {

				$customer = Customer::where('mail', '=', $mail)
									->get()
									->first();
                /*
                $next_order_date = $customer->next_order_date . ' 00:00:00';
                $next_order_date = strtotime($next_order_date);
                */
                $system_date = Carbon::now()->toDateString() . ' 00:00:00';
                $system_date = strtotime($system_date);
                
				if (!is_null($customer->delete_date) && $customer->delete_date <= Carbon::now()) {
					$this->incrementLoginAttempts($request);
					
                    if($lang == "en"){
            			$errors = [ 'mail_en' => 'E-mail address does not exist' ];
                    }else{
            			$errors = [ 'mail' => 'メールアドレスが存在しません' ];
                    }
                   
                    if($lang == "en"){
    					return redirect()
    						->back()
    						->withInput($request->only('mail_en', 'remember'))
    						->withErrors($errors);
    				}else{
    					return redirect()
    						->back()
    						->withInput($request->only('mail', 'remember'))
    						->withErrors($errors);
    				}
				}/*else if(!is_null($customer->next_order_date)){
    				if($next_order_date <= $system_date){
        				$order = Order::find($customer->id);
        				if(empty($order) === true){
            				Auth::guard('mypage')->loginUsingId($customer->id);
            				return redirect(route('mypage.pay', array_merge([ 'lang' => $lang ])));
        				}else{
                            if($lang == "en"){
                    			$errors = [ 'mail_en' => 'This e-mail address can not be logged in' ];
                            }else{
                    			$errors = [ 'mail' => 'このメールアドレスはログインできません' ];
                            }
                            
                            if($lang == "en"){
            					return redirect()
            						->back()
            						->withInput($request->only('mail_en', 'remember'))
            						->withErrors($errors);
            				}else{
            					return redirect()
            						->back()
            						->withInput($request->only('mail', 'remember'))
            						->withErrors($errors);
            				}
        				}
    				}
				}*/

				Auth::guard('mypage')->loginUsingId($customer->id);

				return redirect(route('mypage.report', array_merge([ 'lang' => $lang ])));
			}

			$this->incrementLoginAttempts($request);
            if($lang == "en"){
    			$errors = [ 'mail_en' => 'E-mail address does not exist' ];
            }else{
    			$errors = [ 'mail' => 'メールアドレスが存在しません' ];
            }

            if($lang == "en"){
				return redirect()
					->back()
					->withInput($request->only('mail_en', 'remember'))
					->withErrors($errors);
			}else{
				return redirect()
					->back()
					->withInput($request->only('mail', 'remember'))
					->withErrors($errors);
			}
		}
        
		public function showLoginForm($lang) {

    		if($lang == "en"){
    			return view('mypage.auth.login_en'); //マイページログインページのテンプレート
    		}else{
    			return view('mypage.auth.login'); //マイページログインページのテンプレート
    		}
		}

		/**
		 *
		 * Get the login username to be used by the controller.
		 *
		 * @return string
		 */
		public function username() {
			return 'mail';
		}

		public function logout(Request $request,$lang) {
			$this->guard()->logout();
			$request->session()->flush();
			$request->session()->regenerate();

			return redirect(route('mypage.login', array_merge([ 'lang' => $lang ])));
		}

		protected function guard() {
			return Auth::guard('mypage'); //マイページ認証のguardを指定
		}
	}
