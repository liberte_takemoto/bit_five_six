<?php

	namespace App\Http\Controllers\Mypage;


	use App\Customer;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Crypt;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Http\Request;
	use Carbon\Carbon;


	class CustomerController extends Controller
	{
    	
		/**
		 * CustomerController constructor.
		 */
		public function __construct() {
			$this->middleware('mypage.auth');
		}


		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index($lang) {
    		$authUser = Auth::guard('mypage')->user();
    		/*
    		if(!is_null($authUser->next_order_date) && $authUser->next_order_date . ' 00:00:00' <= Carbon::now()->toDateString() . ' 00:00:00'){
        		return redirect(route('mypage.pay', array_merge([ 'lang' => $lang ])));
    		}
            */
			$customer = Customer::find($authUser->id);
            
            if($lang == "en"){
    			return view('mypage.customer.edit_en', [ 'customer' => $customer ]);
            }else{
    			return view('mypage.customer.edit', [ 'customer' => $customer ]);
            }
		}
		
		
        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request,$lang)
        {
    		$authUser = Auth::guard('mypage')->user();
            
            if($lang == "en"){
        		$this->validate($request, [ 'name1_en'           => 'required|max:50',
        		                            'name2_en'           => 'required|max:50',
        		                            'kana1_en'           => 'required|katakana|max:50',
        		                            'kana2_en'           => 'required|katakana|max:50',
        		                            'zip1_en'            => 'required|numeric|digits:3',
        		                            'zip2_en'            => 'required|numeric|digits:4',
        		                            'addr1_en'           => 'required|max:100',
        		                            'addr2_en'           => 'required|max:100',
        		                            'mail_en'            => 'required|email|unique:customers,mail,'.$authUser->id.',id',
        		                            'en_password'        => 'nullable|confirmed|min:6',
        									'delete_check_en'    => 'nullable|numeric'], 
                                          ['name1_en.required'       => 'Please enter',
                                           'name1_en.max'            => 'Please enter with 50 characters',
                                           'name2_en.required'       => 'Please enter',
                                           'name2_en.max'            => 'Please enter with 50 characters',
                                           'kana1_en.required'       => 'Please enter',
                                           'kana1_en.katakana'       => 'Please input in Katakana',
                                           'kana2_en.required'       => 'Please enter',
                                           'kana2_en.katakana'       => 'Please input in Katakana',
                                           'zip1_en.required'        => 'Please enter',
                                           'zip1_en.numeric'         => 'Please enter with numbers',
                                           'zip1_en.digits'          => 'Please enter with 3 numeric characters',
                                           'zip2_en.required'        => 'Please enter',
                                           'zip2_en.numeric'         => 'Please enter with numbers',
                                           'zip2_en.digits'          => 'Please enter with 4 numeric characters',
                                           'addr1_en.required'       => 'Please enter',
                                           'addr1_en.max'            => 'Please enter with 100 characters',
                                           'addr2_en.required'       => 'Please enter',
                                           'addr2_en.max'            => 'Please enter with 100 characters',
                                           'mail_en.required'        => 'Please enter',
                                           'mail_en.email'           => 'Please enter in the form of e-mail address',
                                           'mail_en.unique'          => 'It is already registered email address',
                                           'en_password.required'    => 'Please enter',
                                           'en_password.confirmed'   => 'Passwords do not match',
                                           'en_password.min'         => 'Please enter at least 6 letters',
                                           'delete_check_en.numeric' => 'Please enter with numbers']);
        									
                $update = Customer::editMypageEn($request,$authUser->id);
                
            }else{
        		$this->validate($request, [ 'name1'           => 'required|max:50',
        		                            'name2'           => 'required|max:50',
        		                            'kana1'           => 'required|katakana|max:50',
        		                            'kana2'           => 'required|katakana|max:50',
        		                            'zip1'            => 'required|numeric|digits:3',
        		                            'zip2'            => 'required|numeric|digits:4',
        		                            'addr1'           => 'required|max:100',
        		                            'addr2'           => 'required|max:100',
        		                            'mail'            => 'required|email|unique:customers,mail,'.$authUser->id.',id',
        		                            'password'        => 'nullable|confirmed|min:6',
        									'delete_check'    => 'nullable|numeric']);
                
                $update = Customer::editMypage($request,$authUser->id);
            }
    
            
            if($lang == "en"){
                /* 英語 */
        		if($update){
        			alert()->success('Success!','Update succeeded');
        		}else{
        			alert()->error('Failed!','Update failed');
        		}
        		
        		if($request->input('delete_check_en') == "1"){
            		return redirect(route('mypage.logout', array_merge([ 'lang' => $lang ])));
        		}else{
            		return redirect(route('mypage.customer', array_merge([ 'lang' => $lang ])));
        		}

            }else{
                /* 日本語 */
        		if($update){
        			alert()->success('成功!','更新に成功しました');
        		}else{
        			alert()->error('失敗!','更新に失敗しました');
        		}
        		
        		if($request->input('delete_check') == "1"){
            		return redirect(route('mypage.logout', array_merge([ 'lang' => $lang ])));
        		}else{
            		return redirect(route('mypage.customer', array_merge([ 'lang' => $lang ])));
        		}
            }
        }
	}
