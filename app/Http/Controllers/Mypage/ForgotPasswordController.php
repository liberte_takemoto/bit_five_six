<?php

	namespace App\Http\Controllers\Mypage;

	use App\Customer;
	use App\Mail\PasswordReset;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Mail;
	use Illuminate\Support\Facades\Password;

	class ForgotPasswordController extends Controller
	{
		/*
	   |--------------------------------------------------------------------------
	   | Password Reset Controller
	   |--------------------------------------------------------------------------
	   |
	   | This controller is responsible for handling password reset emails and
	   | includes a trait which assists in sending these notifications from
	   | your application to your users. Feel free to explore this trait.
	   |
	   */

		use SendsPasswordResetEmails;

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {
			$this->middleware('guest:mypage');
		}

		public function showLinkRequestForm(Request $request,$lang) {
            if($lang == "en"){
    			return view('mypage.passwords.email_en');
            }else{
    			return view('mypage.passwords.email');
            }
		}

		/**
		 * @param Request $request
		 *
		 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function sendResetLinkEmail(Request $request,$lang) {
    		if($lang == "en"){
    			$this->validate($request, [ 'email'            => 'required|email'], 
                                          ['email.required'    => 'Please enter',
                                           'email.email'       => 'Please enter in the form of e-mail address']);
    		}else{
    			$this->validate($request, [ 'email'    => 'required|email']);
    		}

			$customer = Customer::where('mail', '=', $request->input('email'))
								->whereNull('delete_date')
								->get()
								->first();

			if (is_null($customer)) {
    			if($lang == "en"){
    				return redirect()
    					->back()
    					->withInput()
    					->withErrors([ 'email' => 'Member information can not be found. Please check your e-mail address again' ]);
    			}else{
    				return redirect()
    					->back()
    					->withInput()
    					->withErrors([ 'email' => '会員情報が見つかりません。もう一度、メールアドレスをご確認ください' ]);
    			}
			}

			$token = uniqid();

			//PasswordResetsテーブルへ格納
			DB::table('customer_password_resets')
			  ->insert([
							'customer_id' => $customer->id,
							'email'       => $customer->mail,
							'token'       => $token,
							'created_at'  => Carbon::now()->toDateTimeString(),
						]);

			Mail::to($customer->mail)
				->queue(new PasswordReset($lang,$token));

			return redirect(route('mypage.password.email.send', array_merge([ 'lang' => $lang ])));
		}

		public function broker() {
			return Password::broker('customers');
		}

		/**
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function showSendEmail(Request $request,$lang) {
            if($lang == "en"){
    			return view('mypage.passwords.send_en');
            }else{
    			return view('mypage.passwords.send');
            }
		}
	}
