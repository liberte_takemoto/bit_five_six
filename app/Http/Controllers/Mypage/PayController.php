<?php

	namespace App\Http\Controllers\Mypage;

	use App\Customer;
	use App\Order;
	use Carbon\Carbon;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Http\Request;

	class PayController extends Controller
	{
    	
		/**
		 * CustomerController constructor.
		 */
		public function __construct() {
			$this->middleware('mypage.auth');
		}


		/**
		 * 初回決済画面表示
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index($lang) {
    		$authUser = Auth::guard('mypage')->user();

    		if(!is_null($authUser->next_order_date) && $authUser->next_order_date . ' 00:00:00' > Carbon::now()->toDateString() . ' 00:00:00'){
        		return redirect(route('mypage.report', array_merge([ 'lang' => $lang ])));
    		}

			$customer = Customer::find($authUser->id);
            
            if($lang == "en"){
    			return view('mypage.pay.index_en', [ 'customer' => $customer]);
            }else{
    			return view('mypage.pay.index', [ 'customer' => $customer]);
            }
		}
		
		/**
		 * 初回決済処理
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function settlement(Request $request,$lang) {
    		$authUser = Auth::guard('mypage')->user();

			$customer = Customer::find($authUser->id);
            
			$insert = Order::credit($request,$customer);
			
			if (!empty($insert)) {
    			if($lang == "en"){
    			    return view('mypage.pay.thankyou_en');
                }else{
    			    return view('mypage.pay.thankyou');
                }
            }else{
                if($lang == "en"){
        			alert()->error('Failed!','Payment failed. Please check the credit card information.');
        			return view('mypage.pay.index_en', [ 'customer' => $customer]);
                }else{
        			alert()->error('失敗!','お支払いに失敗しました。カード情報をご確認ください。');
        			return view('mypage.pay.index', [ 'customer' => $customer]);
                }
            }
		}
	}
