<?php

	namespace App\Http\Controllers\Mypage;

	use App\Report;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Http\Request;
	use Carbon\Carbon;

	class ReportController extends Controller
	{
    	
		/**
		 * CustomerController constructor.
		 */
		public function __construct() {
			$this->middleware('mypage.auth');
		}


		/**
		 * レポート一覧
		 *
		 * @param Request $request
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function index(Request $request,$lang) {
    		
    		$authUser = Auth::guard('mypage')->user();
            /*
    		if(!is_null($authUser->next_order_date) && $authUser->next_order_date . ' 00:00:00' <= Carbon::now()->toDateString(). ' 00:00:00'){
        		return redirect(route('mypage.pay', array_merge([ 'lang' => $lang ])));
    		}
    		*/
			$reports = Report::orderBy('created_at', 'desc');

            $search = $request->input('search');

            if (!empty($search)) {
                $search_keyword = $request->input('search_keyword');
        			if ($search_keyword <> '') {
    				$reports = $reports->whereIn('id',
    								        function($query) use ($search_keyword)
                                            {
                                               $query->select('report_id')
                                                     ->from('report_keywords')
                                                     ->where('keyword', 'LIKE', "%{$search_keyword}%");
                                            });
    			    }
            }

            $reports = $reports->paginate(config('app.pager_item'));
            
            
            if($lang == "en"){
    			return view('mypage.report.list_en', [ 'report_list' => $reports ]);
            }else{
    			return view('mypage.report.list', [ 'report_list' => $reports ]);
            }
		}

        public function detail($lang,$id) {
    		$authUser = Auth::guard('mypage')->user();
    		/*
    		if(!is_null($authUser->next_order_date) && $authUser->next_order_date . '00:00:00' <= Carbon::now()->toDateString()){
        		return redirect(route('mypage.pay', array_merge([ 'lang' => $lang ])));
    		}
            */
			$report = Report::find($id);
			
			if(is_null($report)){
    			abort('404');
			}
			
            if($lang == "en"){
    			return view('mypage.report.detail_en', [ 'report' => $report ]);
            }else{
    			return view('mypage.report.detail', [ 'report' => $report ]);
            }
        }
	}
