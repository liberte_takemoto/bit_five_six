<?php

	namespace App\Http\Controllers\Mypage;

	use App\Customer;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Foundation\Auth\ResetsPasswords;
	use Illuminate\Support\Facades\DB;

	class ResetPasswordController extends Controller
	{
		/*
		|--------------------------------------------------------------------------
		| Password Reset Controller
		|--------------------------------------------------------------------------
		|
		| This controller is responsible for handling password reset requests
		| and uses a simple trait to include this behavior. You're free to
		| explore this trait and override any methods you wish to tweak.
		|
		*/

		use ResetsPasswords;

		/**
		 * Where to redirect users after resetting their password.
		 *
		 * @var string
		 */
		protected $redirectTo = 'mypage/login';

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct() {
			$this->middleware('guest:mypage');
		}

		/**
		 * @param Request $request
		 * @param null    $token
		 *
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function showResetForm(Request $request, $lang, $token = null) {
			if (is_null($token)) {
				abort('404');
			}

			$limit = Carbon::now()->addMinute(-config('auth.passwords.customers.expire'));
			$tmp   = DB::table('customer_password_resets')
					   ->where('token', '=', $token)
					   ->where('created_at', '>=', $limit->toDateTimeString())
					   ->get()
					   ->first();

			if (is_null($tmp)) {
    			if($lang == "en"){
    				abort('403', 'The token has expired');
    			}else{
    				abort('403', 'トークンの有効期限切れです');
    			}
			}

			$customer = Customer::find($tmp->customer_id);
            
            if($lang == "en"){
    			return view('mypage.passwords.reset_en', [ 'token'      => $token,
    													   'customer'   => $customer]);
            }else{
    			return view('mypage.passwords.reset', [ 'token'      => $token,
    													'customer'   => $customer]);
            }
		}

		/**
		 * @param Request $request
		 *
		 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 */
		public function reset(Request $request, $lang) {
    		if($lang == "en"){
    			$this->validate($request, [ 'email'             => 'required|email',
    			                            'password'          => 'required|confirmed|min:6'], 
                                          ['email.required'     => 'Please enter',
                                           'email.email'        => 'Please enter in the form of e-mail address',
                                           'password.required'  => 'Please enter',
                                           'password.min'       => 'Please enter at least 6 letters',
                                           'password.confirmed' => 'The password does not match the confirmation item.']);
    		}else{
    			$this->validate($request, [ 'email'           => 'required',
    										'password'        => 'required|confirmed|min:6']);
    		}

			if ($request->input('token') == '') {
    			if($lang == "en"){
    				abort('403', 'The token has expired');
    			}else{
    				abort('403', 'トークンの有効期限切れです');
    			}
			}

			$tmp = DB::table('customer_password_resets')
					 ->where('token', '=', $request->input('token'))
					 ->get()
					 ->first();

			$customer = Customer::find($tmp->customer_id);

			if (strcasecmp($customer->mail , $request->input('email')) != 0) {
    			if($lang == "en"){
    				return redirect()
    					->back()
    					->withErrors([ 'email' => 'Mail address does not match' ])
    					->withInput();
    			}else{
    				return redirect()
    					->back()
    					->withErrors([ 'email' => 'メールアドレスが一致しません' ])
    					->withInput();
    			}
			}
            
			$update = Customer::where('id', '=', $customer->id)
							  ->update([ 'password' => bcrypt($request->input('password')) ]);

			if ($update) {
				//customer_password_resetsテーブルから削除
				DB::table('customer_password_resets')
				  ->where('token', '=', $request->input('token'))
				  ->delete();

				return redirect(route('mypage.password.complete', array_merge([ 'lang' => $lang ])));
			} else {
    			if($lang == "en"){
    				return redirect()
    					->back()
    					->withErrors([ 'password' => 'Failed to reset password' ])
    					->withInput();
    			}else{
    				return redirect()
    					->back()
    					->withErrors([ 'password' => 'パスワード再設定に失敗しました' ])
    					->withInput();
    			}
			}
		}

		public function complete(Request $request, $lang) {
    		if($lang == "en"){
    			return view('mypage.passwords.complete_en');
            }else{
    			return view('mypage.passwords.complete');
            }
		}
	}
