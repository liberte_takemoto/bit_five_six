<?php

	namespace App\Http\Middleware;

	use Closure;
	use Illuminate\Support\Facades\Auth;

	class MypageRedirectIfAuthenticated
	{
		/**
		 * Handle an incoming request.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  \Closure                 $next
		 *
		 * @return mixed
		 */
		public function handle($request, Closure $next) {

			if (Auth::guard('mypage')->check()) {
				return redirect('/mypage/report/jp');
			}

			return $next($request);
		}
	}
