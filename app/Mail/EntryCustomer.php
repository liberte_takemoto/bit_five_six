<?php

namespace App\Mail;

use App\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntryCustomer extends Mailable
{
    use Queueable, SerializesModels;

	public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	public function __construct(Customer $customer) {
		$this->customer = $customer;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->subject("【ビット・ファイブ・シックス】会員登録ありがとうございました")
					->view('emails.entrycustomer');
	}
}
