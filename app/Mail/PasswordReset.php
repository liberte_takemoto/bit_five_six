<?php

	namespace App\Mail;

	use Illuminate\Bus\Queueable;
	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;
	use Illuminate\Contracts\Queue\ShouldQueue;

	class PasswordReset extends Mailable
	{
		use Queueable, SerializesModels;

		public $lang;
		public $token;

		/**
		 * Create a new message instance.
		 *
		 * @param $token
		 *
		 * @return void
		 */
		public function __construct($lang,$token) {
			$this->lang  = $lang;
			$this->token = $token;
		}

		/**
		 * Build the message.
		 *
		 * @return $this
		 */
		public function build() {
			$url = route('mypage.password.show.reset.form', [ 'lang' => $this->lang, 'token' => $this->token ]);

			return $this->subject('【ビット・ファイブ・シックス】パスワード再設定')
						->view('emails.mypage_password_reset', [ 'url' => $url ]);
		}
	}
