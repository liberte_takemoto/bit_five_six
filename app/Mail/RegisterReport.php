<?php

namespace App\Mail;

use App\Customer;
use App\Report;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterReport extends Mailable
{
    use Queueable, SerializesModels;

	public $customer;
	public $report;

    /**
     * Create a new message instance.
     *
     * @return void
     */
	public function __construct(Customer $customer,Report $report) {
		$this->customer = $customer;
		$this->report   = $report;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build() {
		return $this->subject("【ビット・ファイブ・シックス】新レポート公開のお知らせ")
					->view('emails.registerreport');
	}
}
