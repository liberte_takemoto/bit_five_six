<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Log;
	use Illuminate\Support\Facades\DB;
	use Payjp\Charge;
	use Payjp\Customer;
	use Payjp\Plan;
	use Payjp\Subscription;
	use Payjp\Payjp;

	class Order extends Model
	{

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable
			= [
				'customer_id',
				'uid',
				'order_date',
				'order_price',
				'created_at',
				'updated_at',
			];
			
			
		/**
		 * @param Request $request
		 *
		 * @return bool
		 */
		static function credit(Request $request, $customer) {

			DB::beginTransaction();
			
			try {
                
                $next_order_date = date('Y-m-d',mktime(0,0,0,date('m'),date('d')+365,date('Y')));
                
                /* 決済情報登録 */
				$order = Order::create(['customer_id'          => $customer->id,
            						    'uid'                  => '',
            						    'order_date'           => Carbon::now(),
            						    'order_price'          => $customer->first_amount,
            						    'created_at'           => Carbon::now()]);
                
				DB::commit();
                
				if (env('APP_ENV') === 'local') {
					Payjp::setApiKey(config('app.payjp-test-secret-key'));
				} else {
					Payjp::setApiKey(config('app.payjp-live-secret-key'));
				}
				
                /* 顧客作成処理 */
                /*
                $payjp_customer = Customer::retrieve($customer->uid);
                
                if(empty($payjp_customer->id) === false){
                    $payjp_customer->delete();
                }
                */
				Customer::create([
    				               'card'     => $request['payjp-token'],
								   'email'    => $customer->mail,
								   'id'       => $customer->uid,
								   'metadata' => [
    								    'customer_id'    => $customer->id,
								   ],
							   ]);
                
                /* 初回決済処理 */
				Charge::create([
    							   'customer' => $customer->uid,
    							   'amount'   => $customer->first_amount,
    							   "currency" => "jpy",
    							   'capture'  => true,
    							   'metadata' => [
    								    'order_id' => $order->id,
    							   ],
    						   ]);
				
				
                /* プラン作成処理 */
				Plan::create([
    				               'amount'     => $customer->regular_amount,
								   'currency'   => "jpy",
								   'interval'   => "year",
								   'id'         => $customer->uid,
								   'metadata'   => [
    								    'customer_id'    => $customer->id,
								   ],
							   ]);
				
                /* 定期購入処理 */
				Subscription::create(['customer'  => $customer->uid,
    								  'plan'      => $customer->uid,
    								  'trial_end' => strtotime($next_order_date.' 00:00:00'),
    								  'metadata'  => [
    								      'customer_id'    => $customer->id,
    								   ],
                                    ]);


				return [ 'order_id' => $order->id ];
				
			} catch (\Exception $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
	}
