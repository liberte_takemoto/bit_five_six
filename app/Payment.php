<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Support\Facades\DB;

	class Payment extends Model
	{

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable
			= [
				'first_amount',
				'regular_amount',
				'regular_day',
				'created_at',
				'updated_at',
			];
			
	}
