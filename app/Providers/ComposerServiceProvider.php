<?php

	namespace App\Providers;

	use Illuminate\Support\Facades\View;
	use Illuminate\Support\ServiceProvider;

	class ComposerServiceProvider extends ServiceProvider
	{
		/**
		 * Bootstrap the application services.
		 *
		 * @return void
		 */
		public function boot() {
			View::composer('admin.*', 'App\Http\Composers\MenuComposer');
			View::composer('mypage.layouts.header', 'App\Http\Composers\HeaderComposer');
			View::composer('mypage.layouts.header_en', 'App\Http\Composers\HeaderComposer');
		}

		/**
		 * Register the application services.
		 *
		 * @return void
		 */
		public function register() {
			//
		}
	}
