<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
		Validator::extend('katakana', function ($attribute, $value, $parameters, $validator) {
			if (preg_match("/^[ァ-ヾ]+$/u", $value)) {
				return true;
			}

			return false;
		});

		Validator::extend('kana', function($attribute, $value, $parameters, $validator) {
			// 半角空白、全角空白、全角記号、全角かなを許可
			return preg_match("/^[ぁ-んー 　！-＠［-｀｛-～]+$/u", $value);
		});
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
