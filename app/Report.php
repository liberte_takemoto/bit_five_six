<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;

	class Report extends Model
	{

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable
			= [
				'title',
				'title_en',
				'content',
				'content_en',
				'created_at',
				'updated_at',
			];
			
		/**
		 * @param Request $request
		 *
		 * @return bool
		 */
		static function store(Request $request) {
			DB::beginTransaction();
			try {
				$report = Report::create([ 'title'       => $request->input('title'),
            							   'title_en'    => $request->input('title_en'),
            							   'content'     => $request->input('content'),
            							   'content_en'  => $request->input('content_en'),
            							   'created_at'  => Carbon::now()]);
    							  
                if(empty($request->input('keyword')) === false){
                    $keyword = explode(",",$request->input('keyword'));
                    
                    foreach($keyword AS $key => $var){
    					DB::table('report_keywords')
    					  ->insert([
								   'report_id'        => $report->id,
								   'keyword'          => $var
    							   ]);
                    }
                }
 
				DB::commit();

				return [ 'report_id' => $report->id ];
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return array();
			}
		}
		
		/**
		 * @param Request $request
		 * @param         $id
		 *
		 * @return bool
		 */
		static function edit(Request $request, $id) {
			DB::beginTransaction();

			try {
				Report::where('id', '=', $id)
					->update([ 'title'       => $request->input('title'),
							   'title_en'    => $request->input('title_en'),
							   'content'     => $request->input('content'),
							   'content_en'  => $request->input('content_en'),
							   'updated_at'  => Carbon::now()]);

                if(empty($request->input('keyword')) === false){
                    DB::table('report_keywords')->where('report_id', '=', $id)->delete();
                    
                    $keyword = explode(",",$request->input('keyword'));
                    
                    foreach($keyword AS $key => $var){
    					DB::table('report_keywords')
    					  ->insert([
								   'report_id'        => $id,
								   'keyword'          => $var
    							   ]);
                    }
                }

				
				DB::commit();

				return true;
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
		
		/**
		 * @param $id
		 *
		 * @return bool
		 */
		static function remove($id) {
			DB::beginTransaction();

			try {

				Report::where('id', '=', $id)
					->delete();

				DB::commit();

				return true;
			} catch (QueryException $exception) {
				Log::error($exception);

				DB::rollback();

				return false;
			}
		}
	}
