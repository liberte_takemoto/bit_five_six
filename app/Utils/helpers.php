<?php
	/**
	 * Created by IntelliJ IDEA.
	 * User: Higuchi Kengo
	 * Date: 2017/08/02
	 * Time: 19:18
	 */

	if (!function_exists('alert')) {
		/**
		 * @param null $title
		 * @param null $message
		 * @return \Illuminate\Foundation\Application|mixed
		 */
		function alert($title = null, $message = null) {
			$flash = app('AlertFlash');
			if (func_num_args() == 0) {
				return $flash;
			}

			return $flash->info($title, $message);
		}
	}

