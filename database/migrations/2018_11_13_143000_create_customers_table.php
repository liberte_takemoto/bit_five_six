<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateCustomersTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			Schema::create('customers', function (Blueprint $table) {
				$table->increments('id');
				$table->string('uid');
				$table->string('subscription_id');
				$table->string('name1',50);
				$table->string('name2',50);
				$table->string('kana1',50);
				$table->string('kana2',50);
				$table->string('zip1',3);
				$table->string('zip2',4);
				$table->string('addr1');
				$table->string('addr2');
				$table->string('mail');
				$table->string('password');
				$table->decimal('first_amount', 8, 0);
				$table->decimal('regular_amount', 8, 0);
				$table->date('next_order_date');
				$table->dateTime('delete_date')->nullable();
				$table->string('remember_token',100);
				$table->timestamps();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			Schema::dropIfExists('customers');
		}
	}
