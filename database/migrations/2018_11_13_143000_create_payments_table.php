<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreatePaymentsTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			Schema::create('payments', function (Blueprint $table) {
				$table->increments('id');
				$table->decimal('first_amount', 8, 0);
				$table->decimal('regular_amount', 8, 0);
				$table->unsignedInteger('regular_day', false)
					  ->default(0);
				$table->timestamps();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			Schema::dropIfExists('payments');
		}
	}
