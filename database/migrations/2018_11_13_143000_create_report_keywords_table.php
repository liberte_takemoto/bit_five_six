<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateReportKeywordsTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			Schema::create('report_keywords', function (Blueprint $table) {
				$table->unsignedInteger('report_id', false)
					  ->default(0);
				$table->string('keyword',100);
				$table->primary([ 'report_id',
								  'keyword']);
			});
			
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			Schema::dropIfExists('report_keywords');
		}
	}
