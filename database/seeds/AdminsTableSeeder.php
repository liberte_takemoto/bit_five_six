<?php

	use Illuminate\Database\Seeder;

	class AdminsTableSeeder extends Seeder
	{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {
			DB::table('admins')->truncate();

			DB::table('admins')->insert([
											[
												'name'       => 'システム管理者',
												'login_id'   => 'freedom',
												'password'   => bcrypt('password'),
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
										]);
		}
	}
