<?php

	use Illuminate\Database\Seeder;

	class CustomersTableSeeder extends Seeder
	{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {
			DB::table('customers')->truncate();

			DB::table('customers')->insert([
											[
												'id'              => 1,
												'uid'             => 'test1',
												'subscription_id' => '',
												'name1'           => 'テスト',
												'name2'           => '太郎',
												'kana1'           => 'テスト',
												'kana2'           => 'タロウ',
												'zip1'            => '664',
												'zip2'            => '0001',
												'addr1'           => '兵庫県伊丹市荒牧',
												'addr2'           => '5-9-13',
												'mail'            => 'takemoto@liberte.blue',
												'password'        => bcrypt('password'),
												'first_amount'    => 60000,
												'regular_amount'  => 100000,
												'next_order_date' => '2018-12-01',
												'remember_token'  => '',
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
											[
												'id'              => 2,
												'uid'             => 'test2',
												'subscription_id' => '',
												'name1'           => 'テスト',
												'name2'           => '二郎',
												'kana1'           => 'テスト',
												'kana2'           => 'ジロウ',
												'zip1'            => '664',
												'zip2'            => '0001',
												'addr1'           => '兵庫県伊丹市荒牧',
												'addr2'           => '5-9-13',
												'mail'            => 'l.blue.k.t@gmail.com',
												'password'        => bcrypt('password'),
												'first_amount'    => 60000,
												'regular_amount'  => 100000,
												'next_order_date' => '2018-12-01',
												'remember_token'  => '',
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
										]);
		}
	}
