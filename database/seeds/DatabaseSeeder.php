<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(AdminsTableSeeder::class);
		$this->call(CustomersTableSeeder::class);
		$this->call(ReportsTableSeeder::class);
		$this->call(ReportKeywordsTableSeeder::class);
		$this->call(PaymentsTableSeeder::class);
    }
}
