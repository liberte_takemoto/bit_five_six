<?php

	use Illuminate\Database\Seeder;

	class PaymentsTableSeeder extends Seeder
	{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {
			DB::table('payments')->truncate();

			DB::table('payments')->insert([
											[
												'id'               => 1,
												'first_amount'     => 60000,
												'regular_amount'   => 100000,
												'regular_day'      => 365,
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
										]);
		}
	}
