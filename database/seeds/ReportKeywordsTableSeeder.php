<?php

	use Illuminate\Database\Seeder;

	class ReportKeywordsTableSeeder extends Seeder
	{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {
			DB::table('report_keywords')->truncate();

			DB::table('report_keywords')->insert([
											[
												'report_id'        => 1,
												'keyword'          => 'test1',
											],
											[
												'report_id'        => 1,
												'keyword'          => 'test2',
											],
											[
												'report_id'        => 1,
												'keyword'          => 'test3',
											],
											[
												'report_id'        => 2,
												'keyword'          => 'test1',
											],
											[
												'report_id'        => 2,
												'keyword'          => 'test2',
											],
											[
												'report_id'        => 2,
												'keyword'          => 'test3',
											],
											[
												'report_id'        => 3,
												'keyword'          => 'test1',
											],
											[
												'report_id'        => 3,
												'keyword'          => 'test2',
											],
											[
												'report_id'        => 3,
												'keyword'          => 'test3',
											],
										]);
		}
	}
