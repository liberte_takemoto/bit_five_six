<?php

	use Illuminate\Database\Seeder;

	class ReportsTableSeeder extends Seeder
	{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {
			DB::table('reports')->truncate();

			DB::table('reports')->insert([
											[
												'id'               => 1,
												'title'            => 'テストレポート１',
												'title_en'         => 'Test Report1',
												'content'          => 'テストレポート１',
												'content_en'       => 'Test Report1',
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
											[
												'id'               => 2,
												'title'            => 'テストレポート２',
												'title_en'         => 'Test Report2',
												'content'          => 'テストレポート２',
												'content_en'       => 'Test Report2',
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
											[
												'id'               => 3,
												'title'            => 'テストレポート３',
												'title_en'         => 'Test Report3',
												'content'          => 'テストレポート３',
												'content_en'       => 'Test Report3',
												'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
											],
										]);
		}
	}
