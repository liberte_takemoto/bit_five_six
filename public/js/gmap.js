  function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 16,
	  center: new google.maps.LatLng(35.6811673,139.7648629),
	  styles: [{
        featureType: 'all',
        elementType: 'all',
        stylers: [{
            hue: '#112649' //16進数のRGB値を記載
        }, {
            saturation: -50 //-100〜100の値を記載
        }, {
            lightness: 0 //-100〜100の値を記載
        }, {
            gamma: 1 //0.01〜10.0の値を記載
        }]
    }]
	});

	setMarkers(map);
  }

  var shops = [
	['株式会社VOLLECT', 35.6811673,139.7648629]
  ];

  function setMarkers(map) {
	var image = {
	  url: 'images/common/img_map.png',
	  size: new google.maps.Size(108,134),
	  origin: new google.maps.Point(0, 0),
	  anchor: new google.maps.Point(54,134),
		  scaledSize: new google.maps.Size(108,134)
	};
	var shape = {
	  coords: [1, 1, 1, 20, 18, 20, 18, 1],
	  type: 'poly'
	};
	for (var i = 0; i < shops.length; i++) {
	  var shop = shops[i];
	  var marker = new google.maps.Marker({
		position: {lat: shop[1], lng: shop[2]},
		map: map,
		icon: image,
		shape: shape,
		title: shop[0],
		zIndex: shop[3]
	  });
	}
  }