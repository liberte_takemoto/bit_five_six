@extends('admin.layouts.admin')
@section('content')
<div id="contents" class="login">

    <!--メインコンテンツ-->
    <div id="main" class="login">
        <h2>ログイン</h2>
        <div class="loginbox">
            <form method="post" action="{{url('/admin/login')}}">
                {{ csrf_field() }}
                <dl>
                    <dt>ログインID</dt>
                    <dd>
                        <input type="text" name="login_id" value="{{old('login_id')}}" required>
                        @if ($errors->has('login_id'))
                            <p class="error tal">{{ $errors->first('login_id') }}</p>
                        @endif
                    </dd>
                </dl>
                <dl>
                    <dt>パスワード</dt>
                    <dd>
                        <input type="password" name="password" required>
                    </dd>
                </dl>
                <input class="btn l w100p mt20" type="submit" value="ログイン">
            </form>
        </div>

    </div><!--main-->

   @include('admin.layouts.footer')
</div>
@endsection