@extends('admin.layouts.admin')
@section('content')
    <div id="contents">
        <div id="main">

            <!--タイトル-->
            <section>
                <div class="title_box2 mb20">
                    <h3>顧客追加</h3>
                </div>
                <form action="{{url(route('admin.customer.store'))}}"
                      method="post">
                    {{csrf_field()}}
                <table class="table2" role="presentation">
                    <tr>
                        <th>お名前<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="name1" style="width:200px;"
                                   value="{{old('name1')}}"
                                   required>
                            <input type="text" name="name2" style="width:200px;"
                                   value="{{old('name2')}}"
                                   required>
                            @if ($errors->has('name2'))
                                <p class="error">{{$errors->first('name2')}}</p>
                            @endif
                            @if ($errors->has('name2'))
                                <p class="error">{{$errors->first('name2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="kana1" style="width:200px;"
                                   value="{{old('kana1')}}"
                                   required>
                            <input type="text" name="kana2" style="width:200px;"
                                   value="{{old('kana2')}}"
                                   required>
                            @if ($errors->has('kana1'))
                                <p class="error">{{$errors->first('kana1')}}</p>
                            @endif
                            @if ($errors->has('kana2'))
                                <p class="error">{{$errors->first('kana2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>郵便番号<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="zip1"
                                   value="{{old('zip1')}}"
                                   class="w50"
                                   required>　ー　
                            <input type="text" name="zip2"
                                   value="{{old('zip2')}}"
                                   class="w50 mr10"
                                   required><input class="btn gray s w80" type="button" id="button" value="住所検索">
                            @if ($errors->has('zip1'))
                                <p class="error">{{$errors->first('zip1')}}</p>
                            @endif
                            @if ($errors->has('zip2'))
                                <p class="error">{{$errors->first('zip2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>住所１<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="addr1"
                                   value="{{old('addr1')}}"
                                   required>
                            @if ($errors->has('addr1'))
                                <p class="error">{{$errors->first('addr1')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>住所２<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="addr2"
                                   value="{{old('addr2')}}"
                                   required>
                            @if ($errors->has('addr2'))
                                <p class="error">{{$errors->first('addr2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="mail"
                                   value="{{old('mail')}}"
                                   required>
                            @if ($errors->has('mail'))
                                <p class="error">{{$errors->first('mail')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>パスワード<span class="hissu">必須</span></th>
                        <td>
                            <input type="password"
                                   name="password"
                                   value=""
                                   placeholder="6文字以上">

                            @if($errors->has('password'))
                                <p class="error">{{ $errors->first('password') }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>パスワード確認用<span class="hissu">必須</span></th>
                        <td>
                            <input id="password-confirm"
                                   type="password"
                                   name="password_confirmation"
                                   placeholder="6文字以上">
                        </td>
                    </tr>
                    <tr>
                        <th>入会日<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="join_date"
                                   value="{{old('join_date')}}"
                                   class="w100 datepicker mr10"
                                   required>
                            @if ($errors->has('join_date'))
                                <p class="error">{{$errors->first('join_date')}}</p>
                            @endif
                        </td>
                    </tr>
                    <!--<tr>
                        <th>初回決済金額<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="first_amount"
                                   value="{{old('first_amount')}}"
                                   required>
                            @if ($errors->has('first_amount'))
                                <p class="error">{{$errors->first('first_amount')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>継続決済金額<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="regular_amount"
                                   value="{{old('regular_amount')}}"
                                   required>
                            @if ($errors->has('regular_amount'))
                                <p class="error">{{$errors->first('regular_amount')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>次回決済日<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="next_order_date"
                                   value="{{old('next_order_date')}}"
                                   class="w100 datepicker mr10"
                                   required>
                            @if ($errors->has('next_order_date'))
                                <p class="error">{{$errors->first('next_order_date')}}</p>
                            @endif
                        </td>
                    </tr>-->
                </table>
            </section>
            <section>
                <div class="tac mt40 mb20">
                    <a href="{{url(route('admin.customer.index'))}}"
                       class="btn l gry3 arrow_l w120 mr10">戻る</a>

                    <input type="submit" value="登録" class="btn l arrow_r w200 mr10">
                </div>
            </form>
            </section>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                buttonImage: '{{asset('images/icn_cal01.png')}}',
                buttonImageOnly: true,
                showOn: "button",
                dateFormat: "yy-mm-dd",
                lang: 'ja',
                inline: true
            });
        });

        $('#button').click(function () {
            AjaxZip3.zip2addr('zip1', 'zip2', 'addr1', 'addr1');
        });

    </script>
@endsection