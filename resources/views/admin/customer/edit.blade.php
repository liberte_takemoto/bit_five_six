@extends('admin.layouts.admin')
@section('content')
    <div id="contents">
        <div id="main">

            <!--タイトル-->
            <section>
                <div class="title_box2 mb20">
                    <h3>顧客編集</h3>
                </div>
                <form action="{{url(route('admin.customer.update',['id' => $customer->id]))}}"
                      method="post">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                <table class="table2" role="presentation">
                    <tr>
                        <th>お名前<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="name1" style="width:200px;"
                                   value="{{!is_null(old('name1')) ? old('name1') : $customer->name1}}"
                                   required>
                            <input type="text" name="name2" style="width:200px;"
                                   value="{{!is_null(old('name2')) ? old('name2') : $customer->name2}}"
                                   required>
                            @if ($errors->has('name1'))
                                <p class="error">{{$errors->first('name1')}}</p>
                            @endif
                            @if ($errors->has('name2'))
                                <p class="error">{{$errors->first('name2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="kana1" style="width:200px;"
                                   value="{{!is_null(old('kana1')) ? old('kana1') : $customer->kana1}}"
                                   required>
                            <input type="text" name="kana2" style="width:200px;"
                                   value="{{!is_null(old('kana2')) ? old('kana2') : $customer->kana2}}"
                                   required>
                            @if ($errors->has('kana1'))
                                <p class="error">{{$errors->first('kana1')}}</p>
                            @endif
                            @if ($errors->has('kana2'))
                                <p class="error">{{$errors->first('kana2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>郵便番号<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="zip1"
                                   value="{{!is_null(old('zip1')) ? old('zip1') : $customer->zip1}}"
                                   class="w50"
                                   required>　ー　
                            <input type="text" name="zip2"
                                   value="{{!is_null(old('zip2')) ? old('zip2') : $customer->zip2}}"
                                   class="w50 mr10"
                                   required><input class="btn gray s w80" type="button" id="button" value="住所検索">
                            @if ($errors->has('zip1'))
                                <p class="error">{{$errors->first('zip1')}}</p>
                            @endif
                            @if ($errors->has('zip2'))
                                <p class="error">{{$errors->first('zip2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>住所１<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="addr1"
                                   value="{{!is_null(old('addr1')) ? old('addr1') : $customer->addr1}}"
                                   required>
                            @if ($errors->has('addr1'))
                                <p class="error">{{$errors->first('addr1')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>住所２<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="addr2"
                                   value="{{!is_null(old('addr2')) ? old('addr2') : $customer->addr2}}"
                                   required>
                            @if ($errors->has('addr2'))
                                <p class="error">{{$errors->first('addr2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="mail"
                                   value="{{!is_null(old('mail')) ? old('mail') : $customer->mail}}"
                                   required>
                            @if ($errors->has('mail'))
                                <p class="error">{{$errors->first('mail')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>パスワード</th>
                        <td>
                            <input type="password"
                                   name="password"
                                   value=""
                                   placeholder="変更する場合のみ記入してください(6文字以上)">

                            @if($errors->has('password'))
                                <p class="error">{{ $errors->first('password') }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>パスワード確認用</th>
                        <td>
                            <input id="password-confirm"
                                   type="password"
                                   name="password_confirmation"
                                   placeholder="変更する場合のみ記入してください(6文字以上)">
                        </td>
                    </tr>
                    <tr>
                        <th>入会日<span class="hissu">必須</span></th>
                        <td>
                            @if(!is_null($customer->join_date))
                                @php
                                    $join_date = date('Y-m-d',strtotime($customer->join_date));
                                @endphp
                            @else
                                @php
                                    $join_date = '';
                                @endphp
                            @endif
                            <input type="text" name="join_date"
                                   value="{{!is_null(old('join_date')) ? old('join_date') : $join_date}}"
                                   class="w100 datepicker mr10"
                                   required>
                            @if ($errors->has('join_date'))
                                <p class="error">{{$errors->first('join_date')}}</p>
                            @endif
                        </td>
                    </tr>
                    <!--<tr>
                        <th>初回決済金額<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="first_amount"
                                   value="{{!is_null(old('first_amount')) ? old('first_amount') : $customer->first_amount}}"
                                   required>
                            @if ($errors->has('first_amount'))
                                <p class="error">{{$errors->first('first_amount')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>継続決済金額<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="regular_amount"
                                   value="{{!is_null(old('regular_amount')) ? old('regular_amount') : $customer->regular_amount}}"
                                   required>
                            @if ($errors->has('regular_amount'))
                                <p class="error">{{$errors->first('regular_amount')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>次回決済日<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="next_order_date"
                                   value="{{!is_null(old('next_order_date')) ? old('next_order_date') : $customer->next_order_date}}"
                                   class="w100 datepicker mr10"
                                   required>
                            @if ($errors->has('next_order_date'))
                                <p class="error">{{$errors->first('next_order_date')}}</p>
                            @endif
                        </td>
                    </tr>-->
                    <tr>
                        <th>退会日</th>
                        <td>
                            @if(!is_null($customer->delete_date))
                                @php
                                    $delete_date = date('Y-m-d',strtotime($customer->delete_date));
                                @endphp
                            @else
                                @php
                                    $delete_date = '';
                                @endphp
                            @endif
                            <input type="text" name="delete_date"
                                   value="{{!is_null(old('delete_date')) ? old('delete_date') : $delete_date}}"
                                   class="w100 datepicker mr10">
                            @if ($errors->has('delete_date'))
                                <p class="error">{{$errors->first('delete_date')}}</p>
                            @endif
                        </td>
                    </tr>
                </table>
            </section>
            <section>
                <div class="tac mt40 mb20">
                    <a href="{{url(route('admin.customer.index'))}}"
                       class="btn l gry3 arrow_l w120 mr10">戻る</a>

                    <input type="submit" value="更新" class="btn l arrow_r w200 mr10">
                </div>
            </form>
            </section>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({
                buttonImage: '{{asset('images/icn_cal01.png')}}',
                buttonImageOnly: true,
                showOn: "button",
                dateFormat: "yy-mm-dd",
                lang: 'ja',
                inline: true
            });
        });

        $('#button').click(function () {
            AjaxZip3.zip2addr('zip1', 'zip2', 'addr1', 'addr1');
        });

    </script>
@endsection