@extends('admin.layouts.admin')
@section('content')
    <div id="contents">
        <div id="main">

            <!--タイトル-->
            <section class="title_box">
                <h2>顧客管理</h2>
            </section>

            <section>
                <form action="{{url(route('admin.customer.index'))}}" method="get">
                    <div class="search_box mb20">
                        <div class="inbox box2">
                            <dl>
                                <dt>氏名・メールアドレス</dt>
                                <dd>
                                    <input type="text" name="search_name" value="{{request('search_name')}}">
                                </dd>
                            </dl>
                        </div>
                        <div class="inbox box2">
                            <dl>
                                <dt>登録日時</dt>
                                <dd>
                                    <input type="text" name="search_create_start" value="{{request('search_create_start')}}" class="w100 mr10 datepicker">　〜　<input type="text" name="search_create_end" value="{{request('search_create_end')}}" class="w100 mr10 datepicker">
                                </dd>
                            </dl>
                        </div>
                    </div>

                    <div class="mt20 tac">
                        <input type="submit" class="btn l w300" value="検索する" name="search">
                        <a class="btn l gry3 w300" href="{{url(route('admin.customer.index'))}}">すべて表示</a>
                    </div>
                </form>
            </section>

            <section>
                <div class="btn_box mb20">
                    <a href="{{url(route('admin.customer.create'))}}"
                       class="btn s plus w150 mr5">新規登録</a>
                </div>
                <div class="pagenav1 tar mb20">
                    {{$customer_list->appends(request()->all())->links()}}
                </div>
                <table class="table1" role="presentation">
                    <tr>
                        <th class="w100">登録日時</th>
                        <th class="w80">氏名</th>
                        <th class="w80">氏名カナ</th>
                        <th class="w200">メールアドレス</th>
                        <th class="w50">ステータス</th>
                        <th class="w50"></th>
                    </tr>
                    @foreach($customer_list AS $customer)
                        <tr>
                            <td class="tac">{{$customer->created_at}}</td>
                            <td class="tal">{{$customer->name1.' '.$customer->name2}}</td>
                            <td class="tal">{{$customer->kana1.' '.$customer->kana2}}</td>
                            <td class="tal">{{$customer->mail}}</td>
                            <td class="tac">
                                @if(!is_null($customer->delete_date))
                                    退会
                                @else
                                    稼働中
                                @endif
                            </td>
                            <td class="tac">
                                <a href="{{url(route('admin.customer.edit',['id' => $customer->id]))}}"
                                   class="btn m arrow_r w60">編集</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </section>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $(".datepicker").datepicker({
                buttonImage: '{{asset('images/icn_cal01.png')}}',
                buttonImageOnly: true,
                showOn: "button",
                dateFormat: "yy-mm-dd",
                lang: 'ja',
                inline: true
            });
        });
    </script>
@endsection