<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=1220,user-scalable=yes">
    <title>{{config('app.name')}}</title>
    <meta name="description" content="{{config('app.name')}}">
    <meta name="keywords" content="{{config('app.name')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/normalize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/base.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/admin.css') }}">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/admin/jquery-ui-timepicker-addon.css') }}">


</head>
<body>

@if (!is_null(Auth::guard('admin')->user()))
    @include('admin.layouts.header')
@endif
@yield('content')

<script src="{{ URL::asset('js/admin/admin.js')}}"></script>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>-->
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script src="{{asset('js/admin/jquery-ui-timepicker-addon.js')}}"></script>
<script src="{{asset('js/admin/jquery.ui.timepicker-ja.js')}}"></script>
@include('admin.alerts')
@yield('scripts')
</body>