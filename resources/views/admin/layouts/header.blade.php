<header>
    <h1>
        <a href="/admin">
        </a>
    </h1>
    <nav>
        @if(!is_null($menus))
            <ul id="normal" class="dropmenu">
                @foreach($menus AS $menu)
                    @if(is_null($menu['sub']))
                        <li class="{{$menu['icon']}}">
                            <a href="{{url(route($menu['router']))}}">{{$menu['name']}}</a>
                        </li>
                    @else
                        <li class="{{$menu['icon']}}"><span>{{$menu['name']}}</span>
                            <ul>
                                @foreach($menu['sub'] AS $sub)
                                    <li><a href="{{url(route($sub['router']))}}">{{$sub['name']}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        @endif
    </nav>
    <div class="name">
        <ul class="dropmenu">
            <li>
                <span> {{ Auth::guard('admin')->user()->name }}</span>
                <ul>
                    <li>
                        <a href="{{url(route('admin.logout'))}}">ログアウト</a>
                    </li>

                </ul>
            </li>
        </ul>
    </div>
</header>