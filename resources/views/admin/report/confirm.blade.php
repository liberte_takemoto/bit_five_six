@extends('admin.layouts.admin')
@section('content')
    <div id="contents">
        <div id="main">
            <section>
                <div class="title_box2 mb20">
                    <h3>注文管理</h3>
                </div>

                <form action="{{url(route('admin.order.thankyou',['id' => $confirm['id']]))}}"
                      method="post">
                    {{csrf_field()}}
                    <table class="table2" role="presentation">
                        <tr>
                            <th>送信先メールアドレス<span class="hissu">必須</span></th>
                            <td>
                                {{$confirm['to_mail']}}
                                <input type="hidden" name="to_mail" value="{{$confirm['to_mail']}}">
                            </td>
                        </tr>
                        <tr>
                            <th>転送先メールアドレス</th>
                            <td>
                                {{$confirm['bcc_mail']}}
                                <input type="hidden" name="bcc_mail" value="{{$confirm['bcc_mail']}}">
                            </td>
                        </tr>
                        <tr>
                            <th>送信元メールアドレス<span class="hissu">必須</span></th>
                            <td>
                                {{$confirm['from_mail']}}
                                <input type="hidden" name="from_mail" value="{{$confirm['from_mail']}}">
                            </td>
                        </tr>
                        <tr>
                            <th>送信元名<span class="hissu">必須</span></th>
                            <td>
                                {{$confirm['from_name']}}
                                <input type="hidden" name="from_name" value="{{$confirm['from_name']}}">
                            </td>
                        </tr>
                        <tr>
                            <th>件名<span class="hissu">必須</span></th>
                            <td>
                                {{$confirm['title']}}
                                <input type="hidden" name="title" value="{{$confirm['title']}}">
                            </td>
                        </tr>
                        <tr>
                            <th>内容<span class="hissu">必須</span></th>
                            <td>
                                {!! nl2br($confirm['content']) !!}
                                <input type="hidden" name="content" value="{!! nl2br($confirm['content']) !!}">
                            </td>
                        </tr>
                    </table>
                    <div class="tac mt40 mb20">
                        <a href="{{url(route('admin.order.mail',['id' => $confirm['id']]))}}"
                           class="btn l gry3 arrow_l w120 mr10">戻る</a>

                        <input type="submit" value="登録" class="btn l arrow_r w200 mr10">
                    </div>
                </form>

            </section>
        </div>
    </div>
@endsection