@extends('admin.layouts.admin')
@section('content')
    <div id="contents">
        <div id="main">

            <!--タイトル-->
            <section>
                <div class="title_box2 mb20">
                    <h3>レポート追加</h3>
                </div>
                <form action="{{url(route('admin.report.store'))}}"
                      method="post">
                    {{csrf_field()}}
                <table class="table2" role="presentation">
                    <tr>
                        <th>タイトル<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="title"
                                   value="{{old('title')}}"
                                   required>
                            @if ($errors->has('title'))
                                <p class="error">{{$errors->first('title')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>タイトル（英語）<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="title_en"
                                   value="{{old('title_en')}}"
                                   required>
                            @if ($errors->has('title_en'))
                                <p class="error">{{$errors->first('title_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>レポート内容<span class="hissu">必須</span></th>
                        <td>
                            <textarea style="height:100px" name="content" id="editor_content">{{old('content')}}</textarea>
                            @if ($errors->has('content'))
                                <p class="error">{{$errors->first('content')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>レポート内容（英語）<span class="hissu">必須</span></th>
                        <td>
                            <textarea name="content_en" id="editor_content_en">{{old('content_en')}}</textarea>
                            @if ($errors->has('content_en'))
                                <p class="error">{{$errors->first('content_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>検索キーワード<span class="hissu">必須</span></th>
                        <td>
                            <input type="text" name="keyword"
                                   value="{{old('keyword')}}"
                                   placeholder="カンマ区切りで入力してください"
                                   required>
                            @if ($errors->has('keyword'))
                                <p class="error">{{$errors->first('keyword')}}</p>
                            @endif
                        </td>
                    </tr>
                </table>
            </section>
            <section>
                <div class="tac mt40 mb20">
                    <a href="{{url(route('admin.report.index'))}}"
                       class="btn l gry3 arrow_l w120 mr10">戻る</a>

                    <input type="submit" value="登録" class="btn l arrow_r w200 mr10">
                </div>
            </form>
            </section>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>
    <script>
    ClassicEditor
      .create( document.querySelector( '#editor_content' ),{
        ckfinder: {
            uploadUrl: '{{url(route('admin.report.image'))}}'
        }
      })
      .then( editor => {
        window.editor = editor;
      })
      .catch( error => {
        console.error( error );
      });
    ClassicEditor
      .create( document.querySelector( '#editor_content_en' ),{
        ckfinder: {
            uploadUrl: '{{url(route('admin.report.image'))}}'
        }
      })
      .then( editor => {
        window.editor = editor;
      })
      .catch( error => {
        console.error( error );
      });
    </script>
@endsection