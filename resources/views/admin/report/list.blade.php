@extends('admin.layouts.admin')
@section('content')
    <div id="contents">
        <div id="main">

            <!--タイトル-->
            <section class="title_box">
                <h2>レポート管理</h2>
            </section>

            <section>
                <form action="{{url(route('admin.report.index'))}}" method="get">
                    <div class="search_box mb20">
                        <div class="inbox box2">
                            <dl>
                                <dt>タイトル</dt>
                                <dd>
                                    <input type="text" name="search_name" value="{{request('search_name')}}">
                                </dd>
                            </dl>
                        </div>
                        <div class="inbox box2">
                            <dl>
                                <dt>登録日時</dt>
                                <dd>
                                    <input type="text" name="search_create_start" value="{{request('search_create_start')}}" class="w100 mr10 datepicker">　〜　<input type="text" name="search_create_end" value="{{request('search_create_end')}}" class="w100 mr10 datepicker">
                                </dd>
                            </dl>
                        </div>
                    </div>

                    <div class="mt20 tac">
                        <input type="submit" class="btn l w300" value="検索する" name="search">
                        <a class="btn l gry3 w300" href="{{url(route('admin.report.index'))}}">すべて表示</a>
                    </div>
                </form>
            </section>

            <section>
                <div class="btn_box mb20">
                    <a href="{{url(route('admin.report.create'))}}"
                       class="btn s plus w150 mr5">新規登録</a>
                </div>

                <div class="pagenav1 tar mb20">
                    {{$report_list->appends(request()->all())->links()}}
                </div>
                <table class="table1" role="presentation">
                    <tr>
                        <th class="w100">登録日時</th>
                        <th class="w200">タイトル</th>
                        <th class="w200">タイトル（英語）</th>
                        <th class="w50"></th>
                    </tr>
                    @foreach($report_list AS $report)
                        <tr>
                            <td class="tac">{{$report->created_at}}</td>
                            <td class="tal">{{$report->title}}</td>
                            <td class="tal">{{$report->title_en}}</td>
                            <td class="tac">
                                <a href="{{url(route('admin.report.edit',['id' => $report->id]))}}"
                                   class="btn m arrow_r w60">編集</a>
                                <button class="btn m arrow_r red1"
                                        onclick="confirmDelete('{{url(route('admin.report.destroy',['id' => $report->id]))}}','{{ csrf_token() }}')"
                                        type="button">削除
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </section>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function () {
            $(".datepicker").datepicker({
                buttonImage: '{{asset('images/icn_cal01.png')}}',
                buttonImageOnly: true,
                showOn: "button",
                dateFormat: "yy-mm-dd",
                lang: 'ja',
                inline: true
            });
        });
    </script>
@endsection