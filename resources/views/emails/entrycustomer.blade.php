{{$customer->name1}} {{$customer->name2}}様<br><br>
この度は、会員登録ありがとうございました。
下記URLより、マイページへログインし、レポートが閲覧できます。<br>
<br>
{{route('mypage.login',['en' => 'jp'])}}<br>
<br>
本アドレスは送信専用です。<br>
返信いただいてもお答えできませんので、ご注意ください。<br>
<br>
----------------------------------------------------------------------<br>
『ビット・ファイブ・シックス』<br>
&copy; {{date('Y')}} {{ config('app.copyright','BitFiveSix') }}
