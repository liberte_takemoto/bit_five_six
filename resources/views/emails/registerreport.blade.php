{{$customer->name1}} {{$customer->name2}}様<br><br>
新レポート【{{$report->title}}】の閲覧が可能になりましたので、お知らせ致します。<br>
マイページへログイン後、下記URLよりレポートをご確認ください<br>
<br>
{{route('mypage.report.detail',['en' => 'jp','id' => $report->id])}}<br>
<br>
本アドレスは送信専用です。<br>
返信いただいてもお答えできませんので、ご注意ください。<br>
<br>
----------------------------------------------------------------------<br>
『ビット・ファイブ・シックス』<br>
&copy; {{date('Y')}} {{ config('app.copyright','BitFiveSix') }}
