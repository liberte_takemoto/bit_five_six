{{$customer->name1}} {{$customer->name2}}様<br><br>
クレジットカード決済が完了致しました。<br>
決済金額：{{number_format($customer->regular_amount)}} 円<br>
<br>
次回決済金額：{{number_format($customer->regular_amount)}} 円<br>
次回決済日：{{$customer->next_order_date}}<br>
<br>
下記URLより、レポートが閲覧できます。<br>
<br>
{{route('mypage.report',['en' => 'jp'])}}<br>
<br>
本アドレスは送信専用です。<br>
返信いただいてもお答えできませんので、ご注意ください。<br>
<br>
----------------------------------------------------------------------<br>
『ビット・ファイブ・シックス』<br>
&copy; {{date('Y')}} {{ config('app.copyright','BitFiveSix') }}
