{{$customer->name1}} {{$customer->name2}}様のクレジットカード決済が失敗しました。<br><br>

顧客ID：{{$customer->uid}}<br>
氏名：{{$customer->name1}} {{$customer->name2}}<br><br>
PayJpの管理画面より、顧客のカード情報を更新していただき、定期課金の次回決済日を変更してください。<br>

<br>
----------------------------------------------------------------------<br>
『ビット・ファイブ・シックス』<br>
&copy; {{date('Y')}} {{ config('app.copyright','BitFiveSix') }}
