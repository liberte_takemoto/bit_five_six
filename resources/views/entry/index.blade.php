@extends('entry.layouts.app')
@section('title','新規会員登録｜ビット・ファイブ・シックス・システム')
@section('content')
    <div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>新規会員登録</h1>
			</div>
		</div>
		
        <div id="main">
			<div class="wrapper">
				<p>
    				必要事項を入力の上、会員登録してください。<br />
                    ご登録から2ヶ月は試用期間として無料で情報を閲覧できます。<br />
                    無料期間終了後、有料会員のご案内をご登録のメールアドレス宛にお送りしますので、ご入会頂ける場合はメールに記載の方法で手続きください。
                </p>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
                <form action="{{url(route('entry.store',['lang' => 'jp']))}}"
                      method="post">
                    {{csrf_field()}}
					<table role="presentation" class="table1">
                        <tr>
                            <th>お名前</th>
                            <td>
                                姓<input type="text" name="name1" class="ml10 mr20 w170" placeholder="例）佐藤"
                                       value="{{old('name1')}}"
                                       required><br class="sp">
                                名<input type="text" name="name2" class="ml10  w170 mt10_sp" placeholder="例）一郎"
                                       value="{{old('name2')}}"
                                       required>
                                @if ($errors->has('name1'))
                                    <p class="error">{{$errors->first('name1')}}</p>
                                @endif
                                @if ($errors->has('name2'))
                                    <p class="error">{{$errors->first('name2')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>フリガナ</th>
                            <td>
                                セイ<input type="text" name="kana1" class="ml10 mr20 w170" placeholder="例）サトウ"
                                       value="{{old('kana1')}}"
                                       required><br class="sp">
                                メイ<input type="text" name="kana2" class="ml10  w170 mt10_sp" placeholder="例）イチロウ"
                                       value="{{old('kana2')}}"
                                       required>
                                @if ($errors->has('kana1'))
                                    <p class="error">{{$errors->first('kana1')}}</p>
                                @endif
                                @if ($errors->has('kana2'))
                                    <p class="error">{{$errors->first('kana2')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>郵便番号</th>
                            <td>
                                <input type="text" name="zip1" id="zip1"
                                       value="{{old('zip1')}}"
                                       maxlength="3"
                                       class="mr10 w80"
                                       placeholder="例）123"
                                       required>ー
                                <input type="text" name="zip2" id="zip2"
                                       value="{{old('zip2')}}"
                                       maxlength="4"
                                       class="ml10 w100"
                                       placeholder="例）0012"
                                       required>
                                @if ($errors->has('zip1'))
                                    <p class="error">{{$errors->first('zip1')}}</p>
                                @endif
                                @if ($errors->has('zip2'))
                                    <p class="error">{{$errors->first('zip2')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td>
                                <input type="text" name="addr1" class="mb20"
                                       value="{{old('addr1')}}"
                                       placeholder="都道府県、市町村区"
                                       required>
                                <input type="text" name="addr2"
                                       value="{{old('addr2')}}"
                                       placeholder="番地、マンション名　など"
                                       required>
                                @if ($errors->has('addr1'))
                                    <p class="error">{{$errors->first('addr1')}}</p>
                                @endif
                                @if ($errors->has('addr2'))
                                    <p class="error">{{$errors->first('addr2')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td>
                                <input type="text" name="mail"
                                       value="{{old('mail')}}"
                                       placeholder="例）bfss@example.com"
                                       required>
                                @if ($errors->has('mail'))
                                    <p class="error">{{$errors->first('mail')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>パスワード</th>
                            <td>
                                <input type="password"
                                       name="password"
                                       value=""
                                       required>
    
                                @if($errors->has('password'))
                                    <p class="error">{{ $errors->first('password') }}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>パスワード確認用</th>
                            <td>
                                <input id="password-confirm"
                                       type="password"
                                       name="password_confirmation"
                                       required>
                            </td>
                        </tr>
                    </table>
                    <div class="btn btn_entry">
                        <input type="submit" value="登録" class="btn1">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        $('#zip1').keyup(function () {
            AjaxZip3.zip2addr('zip1', 'zip2', 'addr1', 'addr1');
        });
        $('#zip2').keyup(function () {
            AjaxZip3.zip2addr('zip1', 'zip2', 'addr1', 'addr1');
        });
    </script>
@endsection