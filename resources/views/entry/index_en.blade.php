@extends('entry.layouts.app_en')
@section('title','新規会員登録｜ビット・ファイブ・シックス・システム')
@section('content')
    <div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Member Registration</h1>
			</div>
		</div>
        <div id="main">
			<div class="wrapper">
				<p>
    				Please input necessary information, please register as a member.<br />
                    You can browse the information for free for 2 months from registration as a trial period.<br />
                    After the end of the free period, we will send information on paid members to the registered e-mail address, so if you can join, please follow the procedure described in the e-mail.
                </p>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
                <form action="{{url(route('entry.store',['lang' => 'en']))}}"
                      method="post">
                    {{csrf_field()}}
                    <table class="table1" role="presentation">
                        <tr>
                            <th>Name</th>
                            <td>
                                <input type="text" name="name1_en" class="ml10 mr20 w170" placeholder="例）佐藤"
                                       value="{{old('name1_en')}}"
                                       required><br class="sp">
                                <input type="text" name="name2_en" class="ml10  w170 mt10_sp" placeholder="例）一郎"
                                       value="{{old('name2_en')}}"
                                       required>
                                @if ($errors->has('name1_en'))
                                    <p class="error">{{$errors->first('name1_en')}}</p>
                                @endif
                                @if ($errors->has('name2_en'))
                                    <p class="error">{{$errors->first('name2_en')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Kana</th>
                            <td>
                                <input type="text" name="kana1_en" class="ml10 mr20 w170" placeholder="例）サトウ"
                                       value="{{old('kana1_en')}}"
                                       required><br class="sp">
                                <input type="text" name="kana2_en" class="ml10  w170 mt10_sp" placeholder="例）イチロウ"
                                       value="{{old('kana2_en')}}"
                                       required>
                                @if ($errors->has('kana1_en'))
                                    <p class="error">{{$errors->first('kana1_en')}}</p>
                                @endif
                                @if ($errors->has('kana2_en'))
                                    <p class="error">{{$errors->first('kana2_en')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>ZipCode</th>
                            <td>
                                <input type="text" name="zip1_en" id="zip1_en"
                                       value="{{old('zip1_en')}}"
                                       maxlength="3"
                                       class="mr10 w80"
                                       placeholder="例）123"
                                       required>　ー　
                                <input type="text" name="zip2_en" id="zip2_en"
                                       value="{{old('zip2_en')}}"
                                       maxlength="4"
                                       class="ml10 w100"
                                       placeholder="例）0012"
                                       required>
                                @if ($errors->has('zip1_en'))
                                    <p class="error">{{$errors->first('zip1_en')}}</p>
                                @endif
                                @if ($errors->has('zip2_en'))
                                    <p class="error">{{$errors->first('zip2_en')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>
                                <input type="text" name="addr1_en"  class="mb20"
                                       value="{{old('addr1_en')}}"
                                       placeholder="Pref、Municipality"
                                       required>
                                <input type="text" name="addr2_en"
                                       value="{{old('addr2_en')}}"
                                       placeholder="Address etc..."
                                       required>
                                @if ($errors->has('addr1_en'))
                                    <p class="error">{{$errors->first('addr1_en')}}</p>
                                @endif
                                @if ($errors->has('addr2_en'))
                                    <p class="error">{{$errors->first('addr2_en')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>
                                <input type="text" name="mail_en"
                                       value="{{old('mail_en')}}"
                                       placeholder="例）bfss@example.com"
                                       required>
                                @if ($errors->has('mail_en'))
                                    <p class="error">{{$errors->first('mail_en')}}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Password</th>
                            <td>
                                <input type="password"
                                       name="en_password"
                                       value=""
                                       required>
    
                                @if($errors->has('en_password'))
                                    <p class="error">{{ $errors->first('en_password') }}</p>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Confirmation Password</th>
                            <td>
                                <input id="password-confirm"
                                       type="password"
                                       name="en_password_confirmation"
                                       required>
                            </td>
                        </tr>
                    </table>
                    <div class="btn btn_entry">
                        <input type="submit" value="Registration" class="btn1">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        $('#zip1_en').keyup(function () {
            AjaxZip3.zip2addr('zip1_en', 'zip2_en', 'addr1_en', 'addr1_en');
        });
        $('#zip2_en').keyup(function () {
            AjaxZip3.zip2addr('zip1_en', 'zip2_en', 'addr1_en', 'addr1_en');
        });
    </script>
@endsection