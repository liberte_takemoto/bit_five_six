<!doctype html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">


    <link rel="apple-touch-icon" href="{{asset('images/webclip.png')}}" />
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/vnd.microsoft.icon">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/base.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">


    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/smoothScroll.js')}}"></script>
    <script src="{{asset('js/jquery.matchHeight.js')}}"></script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/ie9.js"></script>
    <![endif]-->
</head>
<body>
<body id="pagetop">
<div id="fb-root"></div>

<div id="top">
@include('entry.layouts.header')
@yield('content')
@include('entry.layouts.footer')
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
@include('alerts')
@yield('scripts')
</body>
</html>