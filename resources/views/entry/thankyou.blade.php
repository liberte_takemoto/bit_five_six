@extends('mypage.layouts.app')
@section('title','新規会員登録｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>新規会員登録</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
    			@if($error == 0)
				<h2>新規会員登録が完了しました。</h2>
				@else
				<h2>新規会員登録に失敗しました。管理者に問い合わせしてください。</h2>
				@endif
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.login',['lang' => 'jp'])}}">ログイン画面へ</a></div>
            </div>
        </div>
    </div>
@endsection