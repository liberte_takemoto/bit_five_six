@extends('mypage.layouts.app')
@section('title','新規会員登録｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Member Registration</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
    			@if($error == 0)
				<h2>Member registration is completed.</h2>
				@else
				<h2>Member registration failed. Please contact the administrator.</h2>
				@endif
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.login',['lang' => 'jp'])}}">Go to login screen</a></div>
            </div>
        </div>
    </div>
@endsection