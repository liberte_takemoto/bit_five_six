<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" href="{{asset('images/webclip.png')}}" />
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/vnd.microsoft.icon">
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/base.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">


    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/smoothScroll.js')}}"></script>
    <script src="{{asset('js/jquery.matchHeight.js')}}"></script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/ie9.js"></script>
    <![endif]-->
</head>
<body>

<header id="top-head">
	<div class="head-scroll">
		<div class="head_inbox">
			<!-- PC/TB用ヘッダー -->
			<div class="pc-header">
				<div class="logo"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}"><img src="/images/logo.png" alt="ビット・ファイブ・シックス・システム" title="ビット・ファイブ・シックス・システム"></a></div>
                <p class="title">ビット510</p>
                <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
        </div>
    </div>
</div>
<!-- SP用ヘッダー -->
<div class="sp-header">
	<div class="inner">
		<div id="mobile-head">
			<div class="logo"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}"><img src="/images/logo.png" alt="ビット・ファイブ・シックス・システム" title="ビット・ファイブ・シックス・システム"></a></div>
            <div class="head-area02">
                <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
                <p class="title">ビット510</p>
            </div>
			<div id="nav-toggle">
				<div>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>
	</div>
</div>

@yield('content')

<footer>
	<nav>
		<ul>
    		<li></li>
		</ul>
	</nav>
	<p class="copy">{{date('Y').' '.config('app.copyright')}}</p>
</footer>

@yield('scripts')
</body>
</html>