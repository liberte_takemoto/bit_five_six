@extends('error')
@section('title',config('app.name'))
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>ページを表示できません</h1>
			</div>
		</div>

        <div id="main">
            <div class="contents">
                <div class="wrapper">
                    <section>
                        <h3 class="error">{{$exception->getMessage()}}</h3>
                        <div class="box_red mt20">
                        しばらく時間をおいて再度お試しください。<br>
                        時間をおいてもこのメッセージが表示される場合は担当者までご連絡ください。
                        </div>
                    </section>
                </div>
    
            </div>
        </div>
    </div>
@endsection