@extends('error')
@section('title',config('app.name'))
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>システムメンテナンス</h1>
			</div>
		</div>

        <div id="main">
            <div class="contents">
                <div class="wrapper">
                    <section>
                        <h3 class="error">システムメンテナンス</h3>
                        <div class="box_red mt20">
                            現在システムメンテナンス中です<br>
                            メンテナンス終了予定時刻 : 2018/09/02 16:00
                        </div>
                    </section>
                </div>
    
            </div>
        </div>
    </div>
@endsection