<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>仮想通貨1年で5倍に！勝ちパターンご存知ですか？｜ビット510</title>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="満足度93.2%,10倍銘柄の事例多数,独自開発の解析ツール,損をするタイミングは休み、楽して目標達成できる.あなたも億り人を目指しませんか"/>
<meta name="keywords" content="仮想通貨,ビットコイン,投資,はじめて,初心者,勝ちパターン"/>
<meta property="og:title" content="仮想通過1年で5倍に！勝ちパターンご存知ですか？｜ビット510"/>
<meta property="og:url" content=""/>
<meta property="og:image" content=""/>
<link rel="stylesheet" href="{{asset('css/common.css')}}">
<link rel="stylesheet" href="{{asset('css/smart.css')}}">
</head>
<body>
<div id="wrapper">
@include('index.layouts.header')
@yield('content')
@include('index.layouts.footer')
</div>
</body>
</html>