<header class="pc">
  <div class="inner">
    <div class="head-area">
      <h1><a href="/"><img src="/images/logo.png" alt="ロゴ"></a></h1>
      <p class="title">ビット510</p>
      <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
      <p class="right pc"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/header01.png" alt="会員登録はこちら"></a></p>
    </div>
  </div>
</header>
<header class="sp">
  <div class="inner clearfix">
    <div class="head-area">
      <h1><a href="/"><img src="/images/logo_lp.png" alt="ロゴ"></a></h1>
      <div class="head-area02">
        <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
        <p class="title">ビット510</p>
      </div>
      <p class="right"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/sp/header01_sp.png" alt="会員登録はこちら"></a></p>
    </div>
  </div>
</header>