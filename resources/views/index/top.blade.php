@extends('index.layouts.app')
@section('content')
<div id="mainvisual" class="for-fixed">
  <div class="inner">
    <div class="pc">
      <p class="pic"><img src="/images/mainvisual.jpg" alt="価値パターンをご存知ですか"> </p>
    </div>
    <div class="sp">
      <p class="pic"><img src="/images/sp/mainvisual_sp.jpg" alt="価値パターンをご存知ですか"> </p>
    </div>
  </div>
</div>
<article>
<div id="content">
  <div class="pc">
    <section id="sec01" class="sec01 ">
      <p class="cnt pc"><img src="/images/sec01.jpg" alt="１年で５倍に"></p>
      <div class="inner"> </div>
    </section>
    <!--cnv box1 pc-->
    <section id="go_contact" class="go_contact pc">
      <div class="inner">
        <p class="cnt"><img src="/images/cv_cnt.jpg" alt="無料で2ヶ月"></p>
        <p class="btn"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/cv_btn.png" alt="会員登録はこちら"></a></p>
      </div>
    </section>
    <!--cnv box1 pc-->
    <section id="sec02" class="sec02">
      <div class="inner">
        <p class="cnt"><img src="/images/sec02.jpg" alt="成功したい方へ"> </p>
      </div>
    </section>
    <section id="sec03" class="sec03">
      <div class="inner">
        <p class="cnt"><img src="/images/sec03.jpg" alt="満足度93.2％"> </p>
      </div>
    </section>
    <section id="sec04" class="sec04">
      <div class="inner">
        <p class="cnt"><img src="/images/sec04.jpg" alt="選ばれる理由1・2"> </p>
      </div>
    </section>
    <section id="sec05" class="sec05">
      <div class="inner">
        <p class="cnt"><img src="/images/sec05.jpg" alt="選ばれる理由3"> </p>
      </div>
    </section>
    <section id="sec06" class="sec06">
      <div class="inner">
        <p class="cnt"><img src="/images/sec06.jpg" alt="研究員が徹底分析"> </p>
      </div>
    </section>
    <!--cnv box2 pc-->
    <section id="go_contact02" class="go_contact pc">
      <div class="inner">
        <p class="cnt"><img src="/images/cv_cnt.jpg" alt="無料で2ヵ月"></p>
        <p class="btn"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/cv_btn.png" alt="会員登録はこちら"></a></p>
      </div>
    </section>
    <!--cnv box2 pc-->
    <section id="sec07" class="sec07">
      <div class="inner">
        <p class="cnt"><img src="/images/sec07.jpg" alt="お客様の声"> </p>
      </div>
    </section>
    <section id="sec08" class="sec08">
      <div class="inner">
        <p class="cnt"><img src="/images/sec08.jpg" alt="お申し込みの流れ"> </p>
      </div>
    </section>
    <section id="sec09" class="sec09">
      <div class="inner">
        <p class="cnt"><img src="/images/sec09.jpg" alt="よくあるご質問"> </p>
      </div>
    </section>
    <section id="sec10" class="sec10">
      <div class="inner">
        <p class="cnt"><img src="/images/sec10.jpg" alt="あなたも億り人に"> </p>
      </div>
    </section>
    <!--cnv box3 pc-->
    <section id="go_contact03" class="go_contact pc">
      <div class="inner">
        <p class="cnt"><img src="/images/cv_cnt.jpg" alt="無料で2ヵ月"></p>
        <p class="btn"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/cv_btn.png" alt="会員登録はこちら"></a></p>
      </div>
    </section>
    <!--cnv box3 pc--> 
  </div>
  <!-- スマートフォン表示内容 -->
  <div class="sp"> 
    <!--cnv box1 sp-->
    <section id="go_contact" class="go_contact">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/cv_cnt_sp.jpg" alt="無料で2ヵ月"></p>
        <p class="btn"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/sp/cv_btn_sp.png" alt="会員登録はこちら"></a></p>
      </div>
    </section>
    <!--cnv box1 sp-->
    <section id="sec01" class="sec01">
      <div class="inner">
        <p class="cnt sp"><img src="/images/sp/sec01_sp.jpg" alt="１年で５倍に"></p>
      </div>
    </section>
    <section id="sec02_sp" class="sec02">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec02_sp.jpg" alt="成功したい方へ"> </p>
      </div>
    </section>
    <section id="sec03_sp" class="sec03">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec03_sp.jpg" alt="満足度93.2％"> </p>
      </div>
    </section>
    <section id="sec04_sp" class="sec04">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec04_sp.jpg" alt="選ばれる理由1"> </p>
      </div>
    </section>
    <section id="sec05_sp" class="sec05">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec05_sp.jpg" alt="選ばれる理由2"> </p>
      </div>
    </section>
    <section id="sec06_sp" class="sec06">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec06_sp.jpg" alt="選ばれる理由3"> </p>
      </div>
    </section>
    <section id="sec07_sp" class="sec07">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec07_sp.jpg" alt="選ばれる理由3+"> </p>
      </div>
    </section>
    <section id="sec08_sp" class="sec08">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec08_sp.jpg" alt="研究員が徹底分析"> </p>
      </div>
    </section>
    <!--cnv box2 sp-->
    <section id="go_contact" class="go_contact">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/cv_cnt_sp.jpg" alt="無料で2ヵ月"></p>
        <p class="btn"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/sp/cv_btn_sp.png" alt="会員登録はこちら"></a></p>
      </div>
    </section>
    <!--cnv box2 sp-->
    <section id="sec09_sp" class="sec09">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec09_sp.jpg" alt="お客様の声1"> </p>
      </div>
    </section>
    <section id="sec10_sp" class="sec10">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec10_sp.jpg" alt="お客様の声2・3"> </p>
      </div>
    </section>
    <section id="sec11_sp" class="sec11">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec11_sp.jpg" alt="お申し込みの流れ"> </p>
      </div>
    </section>
    <section id="sec12_sp" class="sec12">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec12_sp.jpg" alt="よくあるご質問"> </p>
      </div>
    </section>
    <section id="sec13_sp" class="sec13">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/sec13_sp.jpg" alt="あなたも億り人に"> </p>
      </div>
    </section>
    <!--cnv box3 sp-->
    <section id="go_contact" class="go_contact">
      <div class="inner">
        <p class="cnt"><img src="/images/sp/cv_cnt_sp.jpg" alt="無料で2ヵ月"></p>
        <p class="btn"><a href="https://bit510.net/entry/jp" target="_blank"><img src="/images/sp/cv_btn_sp.png" alt="会員登録はこちら"></a></p>
      </div>
    </section>
    <!--cnv box3 sp--> 
  </div>
@endsection