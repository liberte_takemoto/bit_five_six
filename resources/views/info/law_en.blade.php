@extends('info.layouts.app_en')
@section('title','Law｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Law</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<table role="presentation" class="table3">
					<tr>
						<th>Company name</th>
						<td>SYCC Co.,Ltd.</td>
					</tr>
					<tr>
						<th>Operation manager</th>
						<td>Kazuki Yoshimi</td>
					</tr>
					<tr>
						<th>Company address</th>
						<td>Dai3marukome Bld.503, 4-6-17, Bakuromachi, Chuo-ku Osaka-shi, Osaka, 541-0059, Japan</td>
					</tr>
					<tr>
						<th>Contact information</th>
						<td>
    						Support mail : info@bit510.net<br />
                            Tel:
    				    </td>
					</tr>
					<tr>
						<th>Service fee</th>
						<td>Annual contract first year: 60,000 yen (excluding tax), after second year: 120,000 yen (tax excluded)<br />
                            From the second year onwards it will be a credit card continued billing.
                        </td>
					</tr>
					<tr>
						<th>Payment method</th>
						<td>Credit card</td>
					</tr>
					<tr>
						<th>Service delivery time</th>
						<td>Guide payment within 24 hours after settlement is completed</td>
					</tr>
					<tr>
						<th>Required charge for non-products</th>
						<td>When using a PC<br />
                            Communication fee, provider fee necessary for accessing to various sites provided by this service by the Internet via the Internet, other necessary charges for Internet use<br />
                            <br />
                            When using smartphone<br />
                            The packet communication volume necessary for accessing various sites provided by this service on the smartphone
                        </td>
					</tr>
					<tr>
						<th>About refund / cancellation</th>
						<td>Due to the nature of the service, we do not accept cancellation due to customer's convenience after the start of service provision.</td>
					</tr>
					<tr>
						<th>Unsubscribed</th>
						<td>We are accepting membership withdrawal from member's page.</td>
					</tr>
				</table>
			</div>
		</div>


	</div>
	<!--コンテンツここまで-->
@endsection