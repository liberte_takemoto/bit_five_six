<header id="top-head">
	<div class="head-scroll">
		<div class="head_inbox">
			<!-- PC/TB用ヘッダー -->
			<div class="pc-header">
				<div class="logo"><a href="{{url(route('mypage.report',['lang' => 'en']))}}"><img src="/images/logo.png" alt="ビット・ファイブ・シックス・システム" title="ビット・ファイブ・シックス・システム"></a></div>
                <p class="title">ビット510</p>
                <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
				<div class="head-right">
					<div class="menu">
						<nav>
							<ul>
    							@if($auth->check())
								<li class="mypage"><a href="{{url(route('mypage.report',['lang' => 'en']))}}">Report</a></li>
								<li class="mypage"><a href="{{url(route('mypage.customer',['lang' => 'en']))}}">Member</a></li>
								<li class="logout"><a href="{{url(route('mypage.logout',['lang' => 'en']))}}">Logout</a></li>
								@endif
								<li class="ja"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}">Japanese</a></li>
								<li class="eng"><a href="{{url(route('mypage.report',['lang' => 'en']))}}">English</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
   <!-- SP用ヘッダー -->
	<div class="sp-header">
		<div class="inner">
			<div id="mobile-head">
				<div class="logo"><a href="{{url(route('mypage.report',['lang' => 'en']))}}"><img src="/images/logo.png" alt="ビット・ファイブ・シックス・システム" title="ビット・ファイブ・シックス・システム"></a></div>
                <div class="head-area02">
                    <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
                    <p class="title">ビット510</p>
                </div>
				<div id="nav-toggle">
					<div>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav id="global-nav">
		<section>
			<ul>
				<li><a href="{{url(route('law',['lang' => 'en']))}}">Law</a></li>
				<li><a href="{{url(route('policy',['lang' => 'en']))}}">Policy</a></li>
				<li><a href="{{url(route('rule',['lang' => 'en']))}}">Terms</a></li>
				<li class="tac"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}">Japanese</a>　/　<a href="{{url(route('mypage.report',['lang' => 'en']))}}">English</a></li>
			</ul>
		</section>
	</nav>
</header>
