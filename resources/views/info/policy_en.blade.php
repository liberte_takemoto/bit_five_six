@extends('info.layouts.app_en')
@section('title','Policy｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Policy</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper" id="info">
				<p>SYCC Co.,Ltd. (hereinafter referred to as "our company"), regarding the handling of privacy information in the service provided on this website (hereinafter referred to as "the Service"), the privacy policy , "This policy") will be established.</p>

				<h2>Article 1 (Privacy Information)</h2>
				<ol>
					<li>
"Private information" in the privacy information refers to "personal information" referred to in the Personal Information Protection Law, information on surviving individuals, including name, date of birth, address, phone number , Information that can identify a specific individual by contact information or other description.</li>
					<li>"History information and characteristic information" of privacy information means anything other than "personal information" specified above, and the history of the services you used, the items you purchased, the pages and ads you saw, The search keyword searched, the date and time of use, the method of use, usage environment, postal code and gender, age, IP address of the user, cookie information, location information, individual identification information of the terminal, and so on.</li>
				</ol>

				<h2>Article 2 (Collection method of privacy information)</h2>
				<ol>
					<li>We may ask for personal information such as name, date of birth, address, telephone number, e-mail address, credit card number, etc. when the user makes a registration registration. In addition, transaction records including user's personal information made between users and partners etc. and information on settlement will be sent to our affiliated parties (including information providers, advertisers, advertisement distribution destinations, etc. hereinafter referred to as " Collected from partners ") and so on.</li>
					<li>We will inform you about the services we use, the products purchased, the history of the pages and advertisements you have viewed, the search keywords you have searched, the date and time of use, the usage method, the usage environment (the communication status of that terminal in case you use it through a mobile terminal, It also includes various setting information for use), historical information such as IP address, cookie information, position information, terminal individual identification information, and characteristic information, users can use the services of our company or affiliated parties or browse pages Collect when you do.</li>
				</ol>

				<h2>Article 3 (Purpose of collecting and using personal information)</h2>
				<p>The purpose for which we collect and use personal information is as follows.</p>
				<ul class="indent">
					<li> (1) Register information such as name, address, contact information, payment method, etc., used services, purchased goods, and information on purchased items, in order to allow users to browse and modify their registered information, The purpose of displaying information on such as price etc.</li>
					<li> (2) The purpose of using contact information such as name, address, etc. in case of using e-mail address to inform or communicate to users, sending goods to users or contacting as necessary</li>
					<li> (3) Purpose of using information such as name, date of birth, address, telephone number, achievement result of credit card number, etc. in order to confirm the user's identity</li>
					<li> (4) In order to request the user for the price, information such as the item name and quantity purchased, the type and period of the service used, the number of times, the amount charged, the name, the address, credit card number etc. information such as payment is used the purpose</li>
					<li> (5) In order to allow the user to easily enter data, it is possible to display the information registered in the company on the input screen or to display other services etc. provided by the partner (s) Also include the purpose of transferring</li>
					<li> (6) We refuse the use of users who violate the terms of use of this service, such as delaying payment of payment or causing damage to a third party, or users who intend to use the service for fraudulent / unfair purposes In order to make use of information for identifying an individual such as usage form, name and address etc.</li>
					<li> (7) In order to respond to inquiries from users, information such as contents of inquiries and billing charges etc. necessary for the Company to provide services to users, user service status of users, contact information Purpose of using</li>
					<li>（8) Purpose associated with the above purpose of use</li>
				</ul>

				<h2>Article 4 (Third party provision of personal information)</h2>
				<ol>
					<li>Except for the following cases, we will not provide personal information to third parties without obtaining user consent in advance. However, except as permitted by the Personal Information Protection Act or other laws and regulations.<br>
						<ul class="indent">
							<li>（1）According to laws and regulations</li>
							<li>（2）When it is necessary for the protection of human life, body or property, and it is difficult to obtain the consent of the person himself / herself</li>
							<li>（3）When it is particularly necessary for improving public health or promoting healthy fostering of children and it is difficult to obtain consent of the person himself / herself</li>
							<li>（4）In cases where it is necessary for a national agency or a local public entity or a person entrusted with the consignment to cooperate in carrying out the affairs prescribed by laws and regulations and obtaining consent from the person himself / herself, it will interfere with the execution of the affairs When there is a danger of exerting</li>
							<li>（5）When notifying or announcing the following matters in advance<br>
								<ul class="roman">
									<li>Including provision to third parties for purposes of use</li>
									<li>Items of data provided to third parties</li>
									<li>Means or method of provision to a third party</li>
									<li>Stopping the provision of personal information to third parties as requested by the principal</li>
								</ul>
							</li>
						</ul>
					</li>
					<li>Notwithstanding the provisions of the preceding paragraph, the following cases shall not apply to third parties.<br>
						<ul class="indent">
							<li>（1）When we entrust all or part of the handling of personal information to the extent necessary for achieving the purposes of our company</li>
							<li>（2）When personal information is provided as a result of business succession due to merger or other reasons</li>
							<li>（3）In cases where personal information is to be used jointly with a specific person, that fact, the items of personal information to be used jointly, the range of persons to be used jointly, the purpose of use of persons to use and When notifying the name in advance to the name or name of the person responsible for management of the personal information or putting it in a state that the principal can easily know</li>
						</ul>
					</li>
				</ol>

				<h2>Article 5 (disclosure of personal information)</h2>
				<ol>
					<li>When we are requested to disclose personal information from himself, we will disclose it to himself without delay. However, if it falls under any of the following due to disclosure, it may not disclose all or part of it, and if you decide not to disclose it, you will be notified without delay. For disclosure of personal information, a fee of 1,000 yen will be charged per case.<br>
						<ul class="indent">
							<li>（1）When there is a risk of harming the life, body, property or other rights and interests of the person himself or a third party</li>
							<li>（2）When there is a risk of seriously hindering the proper implementation of our business</li>
							<li>（3）In case of violating other laws and regulations</li>
						</ul>
					</li>
					<li>Notwithstanding the provisions of the preceding paragraph, in principle, we will not disclose information other than personal information such as history information and characteristic information.</li>
				</ol>

				<h2>Article 6 (Correction and deletion of personal information)</h2>
				<ol>
					<li>If the user's personal information owned by the Company is incorrect information, the user can request correction of the personal information or deletion of the personal information by the procedure specified by the Company.</li>
					<li>When we judge that it is necessary to respond to the request by receiving a request from the user from the user, we correct or delete the personal information without delay and notify the user.</li>
				</ol>

				<h2>Article 7 (suspension of use of personal information, etc.)</h2>
				<p>From the person himself / herself, from the reason that personal information is handled beyond the range of purpose of use, or because it is acquired by means of fraud, suspended or eliminated its use (hereinafter referred to as "use If you are asked to stop, etc.), we conduct necessary investigation without delay and stop using the personal information, etc. based on the result and notify him / her to that effect. Provided, however, that if it is difficult to do so, such as when suspension of use of personal information, etc. is expensive, and if it is possible to take alternative measures necessary to protect the rights and interests of the individual , We will take this alternative.</p>

				<h2>Article 8 (Change of Privacy Policy)</h2>
				<ol>
					<li>The contents of this policy can be changed without notifying the user.</li>
					<li>Unless otherwise specified by the Company, the changed privacy policy will take effect from the time it is posted on this website.</li>
				</ol>

				<h2>Article 9 (inquiry window)</h2>
				<p>For inquiries regarding this policy, please contact the following counter.<br>
					Company address：Dai3marukome Bld.503, 4-6-17, Bakuromachi, Chuo-ku Osaka-shi, Osaka, 541-0059, Japan<br>
					Company name：SYCC Co.,Ltd.<br>
					E-mail：info@bit510.net</p>
			</div>
		</div>


	</div>
	<!--コンテンツここまで-->
@endsection