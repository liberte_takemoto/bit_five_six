@extends('info.layouts.app_en')
@section('title','Terms｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Terms</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper" id="info">
				<p>This Terms of Service (the "Terms and Conditions") is a service (hereinafter referred to as "the Service") provided by Dongwoo Corporation (the "Company") on this website It defines the terms of use of. For registered users (hereinafter referred to as "users"), use this service in accordance with these terms.</p>

				<h2>Article 1 (Application)</h2>
				<p>These Terms shall apply to all relationships relating to the use of the Service between you and us.</p>

				<h2>Article 2 (Use registration)</h2>
				<ol>
					<li>Registration applicant applies for registration of use according to the method defined by our company, and our company approves this, and registration of use will be completed.</li>
					<li>We may not approve applications for use registration if we determine that the applicant for use registration has the following reasons and we do not under any obligation to disclose the reason.<br>
						<ul class="indent">
							<li>（1）When notifying of false matters at the time of application for use registration</li>
							<li>（2）In the case of an application from a person who has violated this agreement</li>
							<li>（3）In addition, when we judge that usage registration is not appropriate</li>
						</ul>
					</li>
				</ol>

				<h2>Article 3 (usage fee and payment method)</h2>
				<ol>
					<li>As a consideration for the use of this service, the user shall separately settle and pay the usage fee displayed on this website according to the method specified by the Company.</li>
					<li>In the event that the user has delayed payment of the usage fee, the user shall pay the delayed damages at a rate of 14.6% per year.</li>
				</ol>

				<h2>Article 4 (Prohibited matter)</h2>
				<p>In using the service, the user should not do the following actions.</p>
				<ul class="indent">
					<li>（1）Acts that violate laws or public order and morals</li>
					<li>（2）Acts related to criminal acts</li>
					<li>（3）Actions that destroy or interfere with the functions of our server or network</li>
					<li>（4）Acts that may interfere with the operation of our services</li>
					<li>（5）Acts of collecting or accumulating personal information etc. concerning other users</li>
					<li>（6）Impersonate other users</li>
					<li>（7）Act related to directly or indirectly providing benefits to antisocial forces in connection with our services</li>
					<li>（8）Other acts that we deem inappropriate</li>
				</ul>

				<h2>Article 5 (suspension of provision of this service etc.)</h2>
				<ol>
					<li>The Company shall be able to suspend or suspend the provision of all or part of this service without notifying the user in advance if judging that there is any of the following reasons.<br>
						<ul class="indent">
							<li>（1）When performing maintenance, inspection, or updating of the computer system related to this service</li>
							<li>（2）When it becomes difficult to provide this service due to force majeure such as earthquake, lightning strike, fire, blackout or natural disaster</li>
							<li>（3）When a computer or a communication line stops due to an accident</li>
							<li>（4）In addition, if we determine that it is difficult to provide this service</li>
						</ul>
					</li>
					<li>We shall not bear any responsibility for any disadvantage or damage suffered by users or third parties due to suspension or interruption of the provision of this service, regardless of reason.</li>
				</ol>

				<h2>Article 6 (Limitation on use and deletion of registration)</h2>
				<ol>
					<li>In the following cases, we may restrict the use of all or part of this service to the user without prior notice, or may cancel the registration as a user.<br>
						<ul class="indent">
							<li>（1）In case of violating any provision of this agreement</li>
							<li>（2）When it turns out that there is a false fact in the registration matter</li>
							<li>（3）In addition, when we judge that the use of this service is not appropriate</li>
						</ul>
					</li>
					<li>We are not responsible for any damage caused to the user by the actions our company did under this section.</li>
				</ol>

				<h2>Article 7 (Disclaimer)</h2>
				<ol>
					<li>The Company's obligations to fulfill its obligations shall be exempted from liability if it is not based on our intention or gross negligence.</li>
					<li>We are liable for compensation only within the range of damage that is normally incurred even if we take responsibility for some reason and within the range of payment amount (equivalent to one month in case of continuous service) for paid services Shall be assumed.</li>
					<li>We are not responsible for any transactions, communications or disputes, etc. arising between you and other users or third parties regarding this service.</li>
				</ol>

				<h2>Article 8 (Change of Service Contents, etc.)</h2>
				<p>The Company shall be able to change the contents of this service or cancel the provision of this service without notifying the user and will not bear any responsibility for damage caused to the user by this.</p>

				<h2>Article 9 (Change of Terms of Service)</h2>
				<p>We can change this agreement at any time without notifying the user if we deem it necessary.</p>

				<h2>Article 10 (Notification or contact)</h2>
				<p>The notice or contact between the user and the Company shall be made according to the method specified by the Company.</p>

				<h2>Article 11 (Prohibition of Transfer of Rights and Obligations)</h2>
				<p>The user can not transfer the rights or obligations under the terms of use or third agreement to third parties without prior consent of the Company in writing and can not be used as collateral.</p>

				<h2>Article 12 (Governing Law and Jurisdiction)</h2>
				<ol>
					<li>In interpreting these Terms, the Japanese law shall be the governing law.</li>
					<li>In the event of a dispute with respect to this Service, the court having jurisdiction over the head office location of the Company shall be subject to exclusive agreement jurisdiction.</li>
				</ol>

				<p class="tar mr30">That's all</p>
			</div>
		</div>


	</div>
	<!--コンテンツここまで-->
@endsection