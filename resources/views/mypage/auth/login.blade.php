@extends('mypage.layouts.app')
@section('title','会員ログイン｜ビット・ファイブ・シックス・システム')
@section('content')
<div id="contents">
	<!--title-->
	<div class="main_title">
		<div class="wrapper">
			<h1>会員ログイン</h1>
		</div>
	</div>

    <!--メインコンテンツ-->
    <div id="main">
		<div class="wrapper">
			<p>有料レポートをご覧になるには以下よりログインください。<br>会員でない方は新規会員よりお進みください。</p>
			<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
			<div class="wrapper2">
                <form method="post" action="{{url(route('mypage.login',['lang' => 'jp']))}}">
                    {{ csrf_field() }}
    				<table role="presentation" class="table1">
    					<tr>
    						<th>ID(メールアドレス)</th>
    						<td>
    							<input type="text" name="mail" value="{{old('mail')}}" required>
                                @if ($errors->has('mail'))
                                    <p class="error tal">{{ $errors->first('mail') }}</p>
                                @endif
    						</td>
    					</tr>
    					<tr>
    						<th>パスワード</th>
    						<td>
    							<input type="password" name="password" required>
    						</td>
    					</tr>
    				</table>
    				<div class="btn btn_entry"><input class="btn2" type="submit" value="ログイン"></div>
    				<div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.password.reset',['lang' => 'jp'])}}">パスワードを忘れた方はこちら</a></div>
                </form>
            </div>
			<div class="mt90 mt50_sp">
				<div class="title_line"><span>初めてご利用の方</span></div>
				<div class="btn btn_entry"><a class="btn1" href="{{route('entry',['lang' => 'jp'])}}">新規会員登録</a></div>
			</div>
        </div>

    </div>
    <!--main-->

</div>
@endsection