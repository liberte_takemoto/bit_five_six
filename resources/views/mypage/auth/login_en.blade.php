@extends('mypage.layouts.app_en')
@section('title','会員ログイン｜ビット・ファイブ・シックス・システム')
@section('content')
<div id="contents">
	<!--title-->
	<div class="main_title">
		<div class="wrapper">
			<h1>Login</h1>
		</div>
	</div>

    <!--メインコンテンツ-->
    <div id="main">
		<div class="wrapper">
			<p>To view paid reports please login below.<br>If you are not a member please proceed from the new member.</p>
			<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
			<div class="wrapper2">
                <form method="post" action="{{url(route('mypage.login',['lang' => 'en']))}}">
                    {{ csrf_field() }}
    				<table role="presentation" class="table1">
    					<tr>
    						<th>Email</th>
    						<td>
    							<input type="text" name="mail_en" value="{{old('mail_en')}}" required>
                                @if ($errors->has('mail_en'))
                                    <p class="error tal">{{ $errors->first('mail_en') }}</p>
                                @endif
    						</td>
    					</tr>
    					<tr>
    						<th>Pasword</th>
    						<td>
    							<input type="password" name="en_password" required>
    						</td>
    					</tr>
    				</table>
    				<div class="btn btn_entry"><input class="btn2" type="submit" value="Login"></div>
    				<div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.password.reset',['lang' => 'en'])}}">Click here if you have forgotten your password</a></div>
                </form>
            </div>
			<div class="mt90 mt50_sp">
				<div class="title_line"><span>For first-time users</span></div>
				<div class="btn btn_entry"><a class="btn1" href="{{route('entry',['lang' => 'en'])}}">New member registration</a></div>
			</div>
        </div>

    </div>
    <!--main-->

</div>
@endsection