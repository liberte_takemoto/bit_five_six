@extends('mypage.layouts.app')
@section('title','会員情報変更｜ビット・ファイブ・シックス・システム')
@section('content')
    <div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>会員情報変更</h1>
			</div>
		</div>
		
        <div id="main">
			<div class="wrapper">
				<h2>{{$customer->name1}} {{$customer->name2}} 様の登録情報</h2>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
                <form action="{{url(route('mypage.customer.update',['lang' => 'jp']))}}"
                      method="post">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                <table class="table1" role="presentation">
                    <tr>
                        <th>お名前</th>
                        <td>
                            姓<input type="text" name="name1" class="ml10 mr20 w170" placeholder="例）佐藤"
                                   value="{{!is_null(old('name1')) ? old('name1') : $customer->name1}}"
                                   required><br class="sp">
                            名<input type="text" name="name2" class="ml10  w170 mt10_sp" placeholder="例）一郎"
                                   value="{{!is_null(old('name2')) ? old('name2') : $customer->name2}}"
                                   required>
                            @if ($errors->has('name1'))
                                <p class="error">{{$errors->first('name1')}}</p>
                            @endif
                            @if ($errors->has('name2'))
                                <p class="error">{{$errors->first('name2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>フリガナ</th>
                        <td>
                            セイ<input type="text" name="kana1" class="ml10 mr20 w170" placeholder="例）サトウ"
                                   value="{{!is_null(old('kana1')) ? old('kana1') : $customer->kana1}}"
                                   required><br class="sp">
                            メイ<input type="text" name="kana2" class="ml10  w170 mt10_sp" placeholder="例）イチロウ"
                                   value="{{!is_null(old('kana2')) ? old('kana2') : $customer->kana2}}"
                                   required>
                            @if ($errors->has('kana1'))
                                <p class="error">{{$errors->first('kana1')}}</p>
                            @endif
                            @if ($errors->has('kana2'))
                                <p class="error">{{$errors->first('kana2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>郵便番号</th>
                        <td>
                            <input type="text" name="zip1" id="zip1"
                                   value="{{!is_null(old('zip1')) ? old('zip1') : $customer->zip1}}"
                                   class="mr10 w80"
                                   placeholder="123"
                                   required>ー
                            <input type="text" name="zip2" id="zip2"
                                   value="{{!is_null(old('zip2')) ? old('zip2') : $customer->zip2}}"
                                   class="ml10 w100"
                                   placeholder="0012"
                                   required>
                            @if ($errors->has('zip1'))
                                <p class="error">{{$errors->first('zip1')}}</p>
                            @endif
                            @if ($errors->has('zip2'))
                                <p class="error">{{$errors->first('zip2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>住所</th>
                        <td>
                            <input type="text" name="addr1" placeholder="都道府県、市町村区" class="mb20"
                                   value="{{!is_null(old('addr1')) ? old('addr1') : $customer->addr1}}"
                                   required>
                            <input type="text" name="addr2" placeholder="番地、マンション名　など"
                                   value="{{!is_null(old('addr2')) ? old('addr2') : $customer->addr2}}"
                                   required>
                            @if ($errors->has('addr1'))
                                <p class="error">{{$errors->first('addr1')}}</p>
                            @endif
                            @if ($errors->has('addr2'))
                                <p class="error">{{$errors->first('addr2')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>メールアドレス</th>
                        <td>
                            <input type="text" name="mail" placeholder="例）bfss@example.com"
                                   value="{{!is_null(old('mail')) ? old('mail') : $customer->mail}}"
                                   required>
                            @if ($errors->has('mail'))
                                <p class="error">{{$errors->first('mail')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>パスワード</th>
                        <td>
                            <input type="password"
                                   name="password"
                                   value=""
                                   placeholder="変更する場合のみ記入してください(6文字以上)">

                            @if($errors->has('password'))
                                <p class="error">{{ $errors->first('password') }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>パスワード確認用</th>
                        <td>
                            <input id="password-confirm"
                                   type="password"
                                   name="password_confirmation"
                                   placeholder="変更する場合のみ記入してください(6文字以上)">
                        </td>
                    </tr>
                    <tr>
                        <th>退会</th>
                        <td>
                            <input type="checkbox" name="delete_check" id="delete_check" class="mt10"
                                   value="1">
                            <label for="delete_check">退会する</label>
                            @if ($errors->has('delete_date'))
                                <p class="error">{{$errors->first('delete_date')}}</p>
                            @endif
                        </td>
                    </tr>
                </table>
                <div class="btn btn_entry">
                    <input class="btn1" type="submit" value="変更する">
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
	<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        $('#zip1').keyup(function () {
            AjaxZip3.zip2addr('zip1', 'zip2', 'addr1', 'addr1');
        });
        $('#zip2').keyup(function () {
            AjaxZip3.zip2addr('zip1', 'zip2', 'addr1', 'addr1');
        });
    </script>
@endsection