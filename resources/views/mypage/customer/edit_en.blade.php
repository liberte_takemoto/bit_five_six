@extends('mypage.layouts.app_en')
@section('title','会員情報変更｜ビット・ファイブ・シックス・システム')
@section('content')
    <div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Member</h1>
			</div>
		</div>
        
        <div id="main">
			<div class="wrapper">
				<h2>Registration information</h2>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
                <form action="{{url(route('mypage.customer.update',['lang' => 'en']))}}"
                      method="post">
                    {{method_field('PUT')}}
                    {{csrf_field()}}
                <table class="table1" role="presentation">
                    <tr>
                        <th>Name</th>
                        <td>
                            <input type="text" name="name1_en" class="ml10 mr20 w170" placeholder="例）佐藤"
                                   value="{{!is_null(old('name1_en')) ? old('name1_en') : $customer->name1}}"
                                   required>
                            <input type="text" name="name2_en" class="ml10  w170 mt10_sp" placeholder="例）一郎"
                                   value="{{!is_null(old('name2_en')) ? old('name2_en') : $customer->name2}}"
                                   required>
                            @if ($errors->has('name1_en'))
                                <p class="error">{{$errors->first('name1_en')}}</p>
                            @endif
                            @if ($errors->has('name2_en'))
                                <p class="error">{{$errors->first('name2_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Kana</th>
                        <td>
                            <input type="text" name="kana1_en" class="ml10 mr20 w170" placeholder="例）サトウ"
                                   value="{{!is_null(old('kana1_en')) ? old('kana1_en') : $customer->kana1}}"
                                   required>
                            <input type="text" name="kana2_en" class="ml10  w170 mt10_sp" placeholder="例）イチロウ"
                                   value="{{!is_null(old('kana2_en')) ? old('kana2_en') : $customer->kana2}}"
                                   required>
                            @if ($errors->has('kana1_en'))
                                <p class="error">{{$errors->first('kana1_en')}}</p>
                            @endif
                            @if ($errors->has('kana2_en'))
                                <p class="error">{{$errors->first('kana2_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>ZipCode</th>
                        <td>
                            <input type="text" name="zip1_en" id="zip1_en"
                                   value="{{!is_null(old('zip1_en')) ? old('zip1_en') : $customer->zip1}}"
                                   class="mr10 w80"
                                   placeholder="123"
                                   required>ー
                            <input type="text" name="zip2_en" id="zip2_en"
                                   value="{{!is_null(old('zip2_en')) ? old('zip2_en') : $customer->zip2}}"
                                   class="ml10 w100"
                                   placeholder="0012"
                                   required>
                            @if ($errors->has('zip1_en'))
                                <p class="error">{{$errors->first('zip1_en')}}</p>
                            @endif
                            @if ($errors->has('zip2_en'))
                                <p class="error">{{$errors->first('zip2_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>
                            <input type="text" name="addr1_en" placeholder="Pref、Municipality" class="mb20"
                                   value="{{!is_null(old('addr1_en')) ? old('addr1_en') : $customer->addr1}}"
                                   required>
                            <input type="text" name="addr2_en" placeholder="Address etc..."
                                   value="{{!is_null(old('addr2_en')) ? old('addr2_en') : $customer->addr2}}"
                                   required>
                            @if ($errors->has('addr1_en'))
                                <p class="error">{{$errors->first('addr1_en')}}</p>
                            @endif
                            @if ($errors->has('addr1_en'))
                                <p class="error">{{$errors->first('addr1_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>
                            <input type="text" name="mail_en"  placeholder="例）bfss@example.com"
                                   value="{{!is_null(old('mail_en')) ? old('mail_en') : $customer->mail}}"
                                   required>
                            @if ($errors->has('mail_en'))
                                <p class="error">{{$errors->first('mail_en')}}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Password</th>
                        <td>
                            <input type="password"
                                   name="en_password"
                                   value=""
                                   placeholder="Please fill in only when changing (6 characters or more)">

                            @if($errors->has('en_password'))
                                <p class="error">{{ $errors->first('en_password') }}</p>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Confirmation Password</th>
                        <td>
                            <input id="password-confirm"
                                   type="password"
                                   name="en_password_confirmation"
                                   placeholder="Please fill in only when changing (6 characters or more)">
                        </td>
                    </tr>
                    <tr>
                        <th>Withdraw</th>
                        <td>
                            <input type="checkbox" name="delete_check_en" id="delete_check_en"
                                   value="1">
                            <label for="delete_check_en">Withdraw</label>
                            @if ($errors->has('delete_date_en'))
                                <p class="error">{{$errors->first('delete_date_en')}}</p>
                            @endif
                        </td>
                    </tr>
                </table>
                <div class="btn btn_entry">
                    <input class="btn1" type="submit" value="Update">
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
	<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
    <script>
        $('#zip1_en').keyup(function () {
            AjaxZip3.zip2addr('zip1_en', 'zip2_en', 'addr1_en', 'addr1_en');
        });
        $('#zip2_en').keyup(function () {
            AjaxZip3.zip2addr('zip1_en', 'zip2_en', 'addr1_en', 'addr1_en');
        });
    </script>
@endsection