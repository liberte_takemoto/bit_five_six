	<footer>
		<nav>
			<ul>
				<li><a href="{{url(route('law',['lang' => 'jp']))}}">特別商取引法に基づく表記</a></li>
				<li><a href="{{url(route('policy',['lang' => 'jp']))}}">プライバシーポリシー</a></li>
				<li><a href="{{url(route('rule',['lang' => 'jp']))}}">ご利用規約</a></li>
			</ul>
		</nav>
		<p class="copy">{{date('Y').' '.config('app.copyright')}}</p>
	</footer>
