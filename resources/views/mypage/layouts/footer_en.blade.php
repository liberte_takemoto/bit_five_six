<footer>
	<nav>
		<ul>
			<li><a href="{{url(route('law',['lang' => 'en']))}}">Law</a></li>
			<li><a href="{{url(route('policy',['lang' => 'en']))}}">Policy</a></li>
			<li><a href="{{url(route('rule',['lang' => 'en']))}}">Terms</a></li>
		</ul>
	</nav>
	<p class="copy">{{date('Y').' '.config('app.copyright')}}</p>
</footer>
