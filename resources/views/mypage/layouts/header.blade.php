<?php 
    $query = request()->route()->parameters;
    unset($query['lang']);
?>
<header id="top-head">
	<div class="head-scroll">
		<div class="head_inbox">
			<!-- PC/TB用ヘッダー -->
			<div class="pc-header">
				<div class="logo"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}"><img src="/images/logo.png" alt="ビット・ファイブ・シックス・システム" title="ビット・ファイブ・シックス・システム"></a></div>
                <p class="title">ビット510</p>
                <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
				<div class="head-right">
					<div class="menu">
						<nav>
							<ul>
    							@if($auth->check())
								<li class="mypage"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}">相場レポート</a></li>
								<li class="mypage"><a href="{{url(route('mypage.customer',['lang' => 'jp']))}}">会員情報変更</a></li>
								<li class="logout"><a href="{{url(route('mypage.logout',['lang' => 'jp']))}}">ログアウト</a></li>
								@endif
								<li class="ja"><a href="{{url(route(request()->route()->getName(),array_merge(['lang' => 'jp'],$query)))}}">日本語</a></li>
								<li class="eng"><a href="{{url(route(request()->route()->getName(),array_merge(['lang' => 'en'],$query)))}}">英語</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
   <!-- SP用ヘッダー -->
	<div class="sp-header">
		<div class="inner">
			<div id="mobile-head">
				<div class="logo"><a href="{{url(route('mypage.report',['lang' => 'jp']))}}"><img src="/images/logo.png" alt="ビット・ファイブ・シックス・システム" title="ビット・ファイブ・シックス・システム"></a></div>
                <div class="head-area02">
                    <p class="subtitle">仮想通貨の投資をサポートするレポートサービス</p>
                    <p class="title">ビット510</p>
                </div>
				<div id="nav-toggle">
					<div>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav id="global-nav">
		<section>
			<ul>
				<li><a href="{{url(route('law',['lang' => 'jp']))}}">特別商取引法に基づく表記</a></li>
				<li><a href="{{url(route('policy',['lang' => 'jp']))}}">プライバシーポリシー</a></li>
				<li><a href="{{url(route('rule',['lang' => 'jp']))}}">ご利用規約</a></li>
				<li class="tac"><a href="{{url(route(request()->route()->getName(),array_merge(['lang' => 'jp'],$query)))}}">日本語</a>　/　<a href="{{url(route(request()->route()->getName(),array_merge(['lang' => 'en'],$query)))}}">英語</a></li>
			</ul>
		</section>
	</nav>
</header>
