@extends('mypage.layouts.app')
@section('title','パスワード再設定｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>パスワード再設定</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<h2>パスワードの再設定が完了しました</h2>
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.login',['lang' => 'jp'])}}">ログイン画面へ</a></div>
            </div>
        </div>
    </div>
@endsection