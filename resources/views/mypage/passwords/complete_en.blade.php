@extends('mypage.layouts.app_en')
@section('title','パスワード再設定｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Password Reset</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<h2>Password resetting is completed</h2>
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.login',['lang' => 'en'])}}">Go to login screen</a></div>
            </div>
        </div>
    </div>
@endsection