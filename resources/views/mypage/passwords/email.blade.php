@extends('mypage.layouts.app')
@section('title','会員ログイン｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>会員ログイン</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<h2>パスワードをお忘れの方</h2>
				<p>メールアドレスを入力して「送信する」ボタンを押してください。</p>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
				<div class="wrapper2">
                    <form class="mt10" method="post"
                          action="{{route('mypage.password.email',['lang' => 'jp'])}}">
                    {{ csrf_field() }}
                        <table role="presentation" class="table1">
                            <tr>
                                <th>メールアドレス</th>
                                <td>
                                    <input type="text"
                                           name="email"
                                           value="{{old('email')}}"
                                           required>
                                    @if($errors->has('email'))
                                        <p class="error">{{$errors->first('email')}}</p>
                                    @endif
                                </td>
                            </tr>
                        </table>
                        <div class="btn btn_entry"><input class="btn2" type="submit" value="メールを送信する"></div>
                    </form>
				</div>
            </div>
        </div>
    </div>
@endsection