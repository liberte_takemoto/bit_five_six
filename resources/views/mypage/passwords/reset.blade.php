@extends('mypage.layouts.app')
@section('title','パスワード再設定｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>パスワード再設定</h1>
			</div>
		</div>
		<div id="main">
			<div class="wrapper">
				<p>メールアドレスとパスワードを入力して「パスワードを再設定する」ボタンを押してください。</p>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
				<div class="wrapper2">
                    <form method="post" action="{{route('mypage.password.request',['lang' => 'jp'])}}">
                       {{csrf_field()}}
        
                        <input type="hidden"
                               name="token"
                               value="{{$token}}">
                        <table role="presentation" class="table1">
                            <tr>
                                <th>メールアドレス</th>
                                <td>
                                    <input class=""
                                           type="text"
                                           name="email"
                                           value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p class="error">{{$errors->first('email')}}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>新しいパスワード</th>
                                <td>
                                    <input class=""
                                           type="password"
                                           name="password"
                                           placeholder="半角英数6文字以上">
    
                                    @if($errors->has('password'))
                                        <p class="error">{{$errors->first('password')}}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>新しいパスワード確認用</th>
                                <td>
                                    <input id="password-confirm"
                                           type="password"
                                           class=""
                                           name="password_confirmation"
                                           placeholder="半角英数6文字以上">
                                </td>
                            </tr>
                        </table>
                        <div class="btn btn_entry"><input class="btn2" type="submit" value="パスワードを再設定する"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection