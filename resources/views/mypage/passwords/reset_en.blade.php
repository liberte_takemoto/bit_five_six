@extends('mypage.layouts.app')
@section('title','パスワード再設定｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Password Reset</h1>
			</div>
		</div>
		<div id="main">
			<div class="wrapper">
				<p>Please input e-mail and password and press "Reset Password" button.</p>
				<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
				<div class="wrapper2">
                    <form method="post" action="{{route('mypage.password.request',['lang' => 'en'])}}">
                       {{csrf_field()}}
        
                        <input type="hidden"
                               name="token"
                               value="{{$token}}">
                        <table role="presentation" class="table1">
                            <tr>
                                <th>Email</th>
                                <td>
                                    <input class=""
                                           type="text"
                                           name="email"
                                           value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p class="error">{{$errors->first('email')}}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>New Password</th>
                                <td>
                                    <input class=""
                                           type="password"
                                           name="password">
    
                                    @if($errors->has('password'))
                                        <p class="error">{{$errors->first('password')}}</p>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Confirmation Password</th>
                                <td>
                                    <input id="password-confirm"
                                           type="password"
                                           class=""
                                           name="password_confirmation">
                                </td>
                            </tr>
                        </table>
                        <div class="btn btn_entry"><input class="btn2" type="submit" value="Reset Password"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection