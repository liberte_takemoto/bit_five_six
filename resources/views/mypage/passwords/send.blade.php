@extends('mypage.layouts.app')
@section('title','会員ログイン｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>会員ログイン</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<h2>パスワードをお忘れの方</h2>
                <p>パスワード再設定ページへのリンクを記載したメールをお送りしました。 </p>
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.login',['lang' => 'jp'])}}">ログイン画面へ</a></div>
            </div>
        </div>
    </div>
@endsection