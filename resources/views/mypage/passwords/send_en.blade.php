@extends('mypage.layouts.app_en')
@section('title','会員ログイン｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Login</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
                <p>Sending of mail is completed.</p>
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.login',['lang' => 'en'])}}">Go to login screen</a></div>
            </div>
        </div>
    </div>
@endsection