@extends('mypage.layouts.app_en')
@section('title','お支払い｜ビット・ファイブ・シックス・システム')
@section('content')
<div id="contents">
	<!--title-->
	<div class="main_title">
		<div class="wrapper">
			<h1>Payment</h1>
		</div>
	</div>
	
	<div id="main">
		<div class="wrapper">
			<p>説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト説明テキスト</p>
			<div class="line_dot mt50 mb50 mb20_sp mt20_sp"></div>
			<div class="wrapper2">
				<table role="presentation" class="table2">
					<tr>
						<th>Billing amount</th>
						<td>¥{{number_format($customer->first_amount)}}</td>
					</tr>
					<tr>
						<th>Validity period</th>
						<td>{{date('Y.m.d')}}　〜　{{date('Y.m.d',mktime(0,0,0,date('m'),date('d')+364,date('Y')))}}</td>
					</tr>
				</table>
				<div class="btn btn_entry">
					<form action="{{route('mypage.pay.settlement',array_merge(['lang' => 'en']))}}" method="post">
                        <script src="https://checkout.pay.jp/" class="payjp-button"
                            data-lang="en"
                            data-key="{{env('APP_ENV') === 'production' ? config('app.payjp-live-public-key') : config('app.payjp-test-public-key')}}"></script>
					</form>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection