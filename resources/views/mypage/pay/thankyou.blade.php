@extends('mypage.layouts.app')
@section('title','お支払い｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>お支払い</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<h2>クレジットカード支払いが完了しました。</h2>
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.report',['lang' => 'jp'])}}">レポート画面へ</a></div>
            </div>
        </div>
    </div>
@endsection