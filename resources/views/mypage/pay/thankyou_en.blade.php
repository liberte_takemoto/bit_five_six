@extends('mypage.layouts.app_en')
@section('title','お支払い｜ビット・ファイブ・シックス・システム')
@section('content')
	<!--コンテンツ-->
	<div id="contents">
		<!--title-->
		<div class="main_title">
			<div class="wrapper">
				<h1>Payment</h1>
			</div>
		</div>
		
		<div id="main">
			<div class="wrapper">
				<h2>Credit card settlement is completed</h2>
                <div class="tac mt40 mt30_sp"><a class="link_blue" href="{{route('mypage.report',['lang' => 'en'])}}">Go to report screen</a></div>
            </div>
        </div>
    </div>
@endsection