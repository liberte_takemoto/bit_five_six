@extends('mypage.layouts.app')
@section('title','相場レポート｜ビット・ファイブ・シックス・システム')
@section('content')
<!--コンテンツ-->
<div id="contents">
	<!--title-->
	<div class="main_title">
		<div class="wrapper">
			<h1>相場レポート</h1>
		</div>
	</div>
	
	<div id="main">
		<div class="wrapper" id="report">

			<div class="detail">
				<p class="date">{{date('Y.m.d',strtotime($report->created_at))}}</p>
				<h2>{{$report->title}}</h2>
				{!! $report->content !!}
			</div>
			
			<div class="back">
				<a href="{{route('mypage.report',array_merge(['lang' => 'jp']))}}">一覧に戻る</a>
			</div>
			
		</div>
	</div>
</div>
<!--コンテンツここまで-->
@endsection