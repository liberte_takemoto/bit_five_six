@extends('mypage.layouts.app')
@section('title','相場レポート｜ビット・ファイブ・シックス・システム')
@section('content')
<div id="contents">
	<!--title-->
	<div class="main_title">
		<div class="wrapper">
			<h1>相場レポート</h1>
		</div>
	</div>
	
	<div id="main">
		<div class="wrapper" id="report">
			<div class="search wrapper2">
				<div class="box">
    				<form action="{{route('mypage.report',array_merge(['lang' => 'jp']))}}" method="post">
        				{{ csrf_field() }}
    					<input type="text" name="search_keyword" value="{{request('search_keyword')}}" placeholder="サイト内の記事を検索"><input class="search_btn" type="submit" value="検索" name="search">
					</form>
				</div>
			</div>
			<div class="line_dot mt50 mb40 mb20_sp mt20_sp"></div>
			<div class="list">
				@foreach($report_list AS $report)
				<div class="list_box">
					<a href="{{route('mypage.report.detail',array_merge(['lang' => 'jp', 'id' => $report->id]))}}">
						<div class="date">{{date('Y.m.d',strtotime($report->created_at))}}</div>
						<dl>
							<dt>{{$report->title}}</dt>
							<dd></dd>
						</dl>
					</a>
				</div>
				@endforeach
			</div>
        </div>
    </div>
</div>
@endsection