@extends('mypage.layouts.app_en')
@section('title','相場レポート｜ビット・ファイブ・シックス・システム')
@section('content')
<div id="contents">
	<!--title-->
	<div class="main_title">
		<div class="wrapper">
			<h1>Report</h1>
		</div>
	</div>
	
	<div id="main">
		<div class="wrapper" id="report">
			<div class="search wrapper2">
				<div class="box">
    				<form action="{{route('mypage.report',array_merge(['lang' => 'en']))}}" method="post">
        				{{ csrf_field() }}
    					<input type="text" name="search_keyword" value="{{request('search_keyword')}}" placeholder="Search articles in the site"><input class="search_btn" type="submit" value="Search" name="search">
					</form>
				</div>
			</div>
			<div class="line_dot mt50 mb40 mb20_sp mt20_sp"></div>
			<div class="list">
				@foreach($report_list AS $report)
				<div class="list_box">
					<a href="{{route('mypage.report.detail',array_merge(['lang' => 'en', 'id' => $report->id]))}}">
						<div class="date">{{date('Y.m.d',strtotime($report->created_at))}}</div>
						<dl>
							<dt>{{$report->title_en}}</dt>
							<dd></dd>
						</dl>
					</a>
				</div>
				@endforeach
			</div>
        </div>
    </div>
</div>
@endsection