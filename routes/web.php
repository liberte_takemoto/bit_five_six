<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	//トップページLP
	Route::get('/', 'HomeController@top')->name('top');

	//特定商取引法に基づく表記
	Route::get('/law/{lang}', 'HomeController@law')->name('law');
	//プライバシーポリシー
	Route::get('/policy/{lang}', 'HomeController@policy')->name('policy');
	//利用規約
	Route::get('/rules/{lang}', 'HomeController@rule')->name('rule');

	//会員登録
	Route::get('/entry/{lang}', 'EntryController@index')->name('entry');
	//会員登録完了
	Route::post('/entry/{lang}/store', 'EntryController@store')->name('entry.store');

	Route::get('/entry/{lang}/thankyou', 'EntryController@thankyou')->name('entry.thankyou');

	//クレジットカード支払い決済通知
	Route::post('callback/credit', 'Callback\CreditController@index')->name('callback.credit');
	Route::get('callback/credit', function () {
		Log::info('ログ出力');
		abort(500, '権限がありません');
	});

    /* マイページ ログイン前 */
	Route::group([ 'prefix'     => 'mypage',
				   'namespace'  => 'Mypage'], function () {
		Route::get('/', function () {
			return redirect('/mypage/login/jp');
		});

    	//マイページ認証処理
    	Route::get('login/{lang}', 'Auth\LoginController@showLoginForm')->name('mypage.login');
    	Route::post('login/{lang}', 'Auth\LoginController@login')->name('mypage.signin');
		Route::get('password/{lang}/reset', 'ForgotPasswordController@showLinkRequestForm')->name('mypage.password.reset');
		Route::post('password/{lang}/email', 'ForgotPasswordController@sendResetLinkEmail')->name('mypage.password.email');
		Route::get('password/{lang}/email/send', 'ForgotPasswordController@showSendEmail')->name('mypage.password.email.send');
		Route::get('password/{lang}/reset/{token}', 'ResetPasswordController@showResetForm')
			 ->name('mypage.password.show.reset.form');
		Route::post('password/{lang}/reset', 'ResetPasswordController@reset')->name('mypage.password.request');
		Route::get('password/{lang}/complete', 'ResetPasswordController@complete')->name('mypage.password.complete');
        
        /* マイページ ログイン後 */
		Route::group([ 'guard'      => 'mypage',
					   'middleware' => 'mypage.auth' ], function () {
			Route::get('logout/{lang}', 'Auth\LoginController@logout')->name('mypage.logout');
			Route::post('logout/{lang}', 'Auth\LoginController@logout')->name('mypage.logout');


			Route::get('report/{lang}', 'ReportController@index')->name('mypage.report');
			Route::post('report/{lang}', 'ReportController@index')->name('mypage.report');
			Route::get('report/{lang}/detail/{id}', 'ReportController@detail')->name('mypage.report.detail');
			
    		Route::resource('report', 'ReportController', [ 'names' => [ 'index'   => 'mypage.report.index',
        																 'store'   => 'mypage.report.store',
        																 'create'  => 'mypage.report.create',
        																 'destroy' => 'mypage.report.destroy',
        																 'update'  => 'mypage.report.update',
        																 'show'    => 'mypage.report.show',
        																 'edit'    => 'mypage.report.edit' ] ]);
			
			Route::get('customer/{lang}', 'CustomerController@index')->name('mypage.customer');
			Route::post('customer/{lang}/update', 'CustomerController@update')->name('mypage.customer.update');
			
    		Route::resource('customer', 'CustomerController', [ 'names' => [ 'index'   => 'mypage.customer.index',
            																 'store'   => 'mypage.customer.store',
            																 'create'  => 'mypage.customer.create',
            																 'destroy' => 'mypage.customer.destroy',
            																 'update'  => 'mypage.customer.update',
            																 'show'    => 'mypage.customer.show',
            																 'edit'    => 'mypage.customer.edit' ] ]);
            																 
			Route::get('pay/{lang}', 'PayController@index')->name('mypage.pay');
			Route::get('pay/{lang}/settlement', 'PayController@index')->name('mypage.pay.settlement');
			Route::post('pay/{lang}/settlement', 'PayController@settlement')->name('mypage.pay.settlement');
			
    		Route::resource('pay', 'PayController', [ 'names' => [ 'index'   => 'mypage.pay.index',
        												           'store'   => 'mypage.pay.store',
        												           'create'  => 'mypage.pay.create',
        												           'destroy' => 'mypage.pay.destroy',
        												           'update'  => 'mypage.pay.update',
        												           'show'    => 'mypage.pay.show',
        												           'edit'    => 'mypage.pay.edit' ] ]);

		});
        
	});

	Route::group([ 'prefix'     => 'admin',
				   'namespace'  => 'Admin',
				   'middleware' => 'admin.auth' ], function () {
		Route::get('/', function () {
			return redirect('/admin/customer');
		});
		Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
		Route::post('login', 'Auth\LoginController@login')->name('admin.login');
		Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

		//会員管理
		Route::resource('customer', 'CustomerController', [ 'names' => [ 'index'    => 'admin.customer.index',
    																     'store'    => 'admin.customer.store',
    																     'create'   => 'admin.customer.create',
    																     'destroy'  => 'admin.customer.destroy',
    																     'update'   => 'admin.customer.update',
    																     'show'     => 'admin.customer.show',
    																     'edit'     => 'admin.customer.edit' ] ]);

		//レポート管理
		Route::post('report/image', 'ReportController@uploadImage')
			 ->name('admin.report.image');
		
		Route::resource('report', 'ReportController', [ 'names' => [ 'index'   => 'admin.report.index',
    																 'store'   => 'admin.report.store',
    																 'create'  => 'admin.report.create',
    																 'destroy' => 'admin.report.destroy',
    																 'update'  => 'admin.report.update',
    																 'show'    => 'admin.report.show',
    																 'edit'    => 'admin.report.edit' ] ]);

	});